CREATE TABLE `battingdata` (
  `BattingDataID` int NOT NULL,
  `Match_id` int DEFAULT NULL,
  `PlayerID` int DEFAULT NULL,
  `RunsScored` int DEFAULT NULL,
  `BallsFaced` int DEFAULT NULL,
  `StrikeRate` decimal(5,2) DEFAULT NULL,
  `DismissalType` varchar(50) DEFAULT NULL,
  `playername` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`BattingDataID`),
  KEY `Match_id` (`Match_id`),
  KEY `PlayerID` (`PlayerID`),
  CONSTRAINT `battingdata_ibfk_1` FOREIGN KEY (`Match_id`) REFERENCES `matches` (`Match_id`),
  CONSTRAINT `battingdata_ibfk_2` FOREIGN KEY (`PlayerID`) REFERENCES `players` (`PlayerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci