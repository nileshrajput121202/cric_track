CREATE TABLE `matches` (
  `Match_id` int NOT NULL AUTO_INCREMENT,
  `Team1` varchar(45) DEFAULT NULL,
  `Team2` varchar(45) DEFAULT NULL,
  `TossWinner` varchar(45) DEFAULT NULL,
  `MatchTime` varchar(45) DEFAULT NULL,
  `MatchLocation` varchar(45) DEFAULT NULL,
  `Winner` varchar(45) DEFAULT NULL,
  `MatchFormat` varchar(45) DEFAULT NULL,
  `Overs` int DEFAULT NULL,
  PRIMARY KEY (`Match_id`)
) ENGINE=InnoDB AUTO_INCREMENT=998 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci