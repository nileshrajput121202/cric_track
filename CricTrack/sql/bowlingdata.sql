CREATE TABLE `bowlingdata` (
  `BowlingDataID` int NOT NULL,
  `Match_id` int DEFAULT NULL,
  `PlayerID` int DEFAULT NULL,
  `OversBowled` decimal(4,1) DEFAULT NULL,
  `WicketsTaken` int DEFAULT NULL,
  `RunsConceded` int DEFAULT NULL,
  `EconomyRate` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`BowlingDataID`),
  KEY `Match_id` (`Match_id`),
  KEY `PlayerID` (`PlayerID`),
  CONSTRAINT `bowlingdata_ibfk_1` FOREIGN KEY (`Match_id`) REFERENCES `matches` (`Match_id`),
  CONSTRAINT `bowlingdata_ibfk_2` FOREIGN KEY (`PlayerID`) REFERENCES `players` (`PlayerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci