

import Models.CreateMatch;
import Models.aboutus;
import javafx.animation.*;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import javafx.stage.Stage;
import javafx.util.Duration;

 

public class StartApp extends Application {

    Stage window;

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Start Application");

        GridPane dGridPane = new GridPane();
        dGridPane.setPadding(new Insets(50, 1, 1, 1));
        dGridPane.setAlignment(Pos.CENTER);

        // Load and display an image at the top-left corner with rotational transition
        Image logoImage = new Image("Images/c2w.png");
        ImageView logoImageView = new ImageView(logoImage);
        logoImageView.setFitWidth(100); // Set the desired width
        logoImageView.setFitHeight(100);
        // RotateTransition rotateTransition = new RotateTransition(Duration.seconds(1), logoImageView);
        // rotateTransition.setByAngle(50); // Rotate 360 degrees
        // rotateTransition.setCycleCount(Animation.INDEFINITE);
        // rotateTransition.setAutoReverse(true);
        // rotateTransition.play();

        dGridPane.add(logoImageView, 0, 1); // Position at the top-left corner

        //Title at the top-middle with top-to-down transition
        StackPane titlePane = createAnimatedTitle();
        GridPane.setConstraints(titlePane, 0, 1); // Position below the image
        dGridPane.getChildren().add(titlePane);

        // Cricket-related quote (subtitle) with left-to-right transition
        Text quote = new Text("'CRICKET IS NOT JUST A SPORT, IT'S A WAY OF LIFE...!'");
        quote.setFont(Font.font("Arial", 50));
        quote.setFill(Color.BLACK);
        GridPane.setConstraints(quote, 0, 2); // Position just below the title
        dGridPane.getChildren().add(quote);

        // Button animations (Top to Bottom transition)
        Button startButton = new Button("Start");
        startButton.setStyle("-fx-font-size: 20px;");
        GridPane.setConstraints(startButton, 1, 70); // Position just below the subtitle
        dGridPane.getChildren().add(startButton);
        TranslateTransition topToBottomStartButton = createTopToBottomTransition(startButton);

        Button aboutUsButton = new Button("About Us");
        aboutUsButton.setStyle("-fx-font-size: 20px;");
        GridPane.setConstraints(aboutUsButton, 0, 70); // Position at the bottom-left
        dGridPane.getChildren().add(aboutUsButton);
        TranslateTransition topToBottomAboutUsButton = createTopToBottomTransition(aboutUsButton);

        

        // Load your background image
        Image backgroundImage = new Image("Images/Cricket DashBoard 1.jpg");
        BackgroundImage background = new BackgroundImage(backgroundImage,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(1950, 1000, false, false, false, false));

        dGridPane.setHgap(10);
        dGridPane.setVgap(10);
        dGridPane.setAlignment(Pos.CENTER);

        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(dGridPane);

        VBox container = new VBox(50);
        container.getChildren().addAll(borderPane);
        container.setBackground(new Background(background));

        Scene scene = new Scene(container, 1930, 1000);
        scene.getStylesheets().add("style.css");

        startButton.getStyleClass().add("button");
        aboutUsButton.getStyleClass().add("button");

        startButton.setOnAction(e -> {
            System.out.println("You Click On Start Button");
            try {
                CreateMatch.Display(window, scene);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });
        aboutUsButton.setOnAction(e -> {
            System.out.println("You Click On Start Button");
            try {
                aboutus.about();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        // You can add an action for the "About Us" button here if needed

        window.setScene(scene);
        window.show();

        // Start the animations
        topToBottomStartButton.play();
        topToBottomAboutUsButton.play();
        
        // Start the left-to-right transition for the quote
        TranslateTransition leftToRightTransition = new TranslateTransition(Duration.seconds(2), quote);
        leftToRightTransition.setFromX(-600); // Start from the left of the screen
        leftToRightTransition.setToX(0);      // End at the original position
        leftToRightTransition.setCycleCount(1);
        leftToRightTransition.play();
    }

    private StackPane createAnimatedTitle() {
        StackPane titlePane = new StackPane();
        Text title = new Text("CRICTRACK");
        title.setFont(Font.font("Serif", FontWeight.BOLD, 80));
        title.setFill(Color.BLACK);

        // Add a drop shadow effect
        DropShadow dropShadow = new DropShadow();
        dropShadow.setColor(Color.WHITE);
        dropShadow.setRadius(10);
        title.setEffect(dropShadow);

        // Create a radial gradient for the title
        RadialGradient radialGradient = new RadialGradient(
                0, 0, 0.5, 0.5, 0.7, false, CycleMethod.NO_CYCLE,
                new Stop(1, Color.BLACK),
                new Stop(1, Color.DARKBLUE)
        );
        title.setFill(radialGradient);

        titlePane.getChildren().add(title);

        // Create a top-to-down transition for the title
        TranslateTransition tt = new TranslateTransition(Duration.seconds(2), title);
        tt.setFromY(-100);  // Start from above the screen
        tt.setToY(0);       // End at the original position
        tt.setCycleCount(1);

        // Play the top-to-down transition
        tt.play();

        return titlePane;
    }

    private TranslateTransition createTopToBottomTransition(Button button) {
        TranslateTransition translateTransition = new TranslateTransition(Duration.seconds(1), button);
        translateTransition.setFromY(-100);
        translateTransition.setToY(0);
        return translateTransition;
    }
}



