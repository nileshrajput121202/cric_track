package Models;



import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;



public class inning2{
    static Label dataEntryLabel;
    static TextField runsInput;
    static Button submitButton;
    static Label scoreboardLabel;
    static TextArea scoreboardTextArea;
    static Stage window;

    static Button oneRunButton;
    static Button dotButton;
    static Button twoRunButton;
    static Button undoButton;
    static Button threeRunButton;
    static Button fourRunButton;
    static Button fiveRunButton;
    static Button sixRunButton;
    static Button noBallButton;
    static Button wicketButton;    
    static Button byesBallButton;
    static MenuButton wideMenuButton;
    static MenuButton byesMenuButton;
    static MenuButton wicketMenuButton;
    static MenuButton noBallMenuButton;
    static MenuButton strikeMenuButton;

    static TextArea runsTextArea;
    static TextArea player1RunsTextArea;
    static TextArea player2RunsTextArea;
    static TextArea runRateTextArea;
    static TextArea wicketsTextArea;
    static TextArea bowler1InfoTextArea;
    static TextArea bowler2InfoTextArea;
    static TextArea lastSixBallsTextArea;
    static VBox scoreboardLayout;
    static SplitPane splitPane;
    static VBox dataEntryLayout;
    static HBox nameScoreVBox;
    static HBox battersBox;
    static VBox lastVBox;
    static VBox dataVBox;
    static HBox runsHBox1;
    static HBox runsHBox2;
    static HBox strikeHBox;
    static Scene scene;
    static Label scoreLabel;
    static int wickets = 0;
    static int over = 0;
    static int balls = 0;
    static String Batsman1 = "";
    static String Batsman2 = "";
    static int StrikeTemp = 0 ;
    static int BowlerTemp = 0;
    static String BowlerName1 = "";
    static String BowlerName2 = "";
    static int Runs= 0;
    static int Batsman1runs  = 0;
    static int Batsman2runs  = 0;
    static int Batsman1balls  = 0;
    static int Batsman2balls  = 0;

    static int Batsman1fours = 0;
    static int Batsman2fours = 0;

    static double Batsman1StrikeR = 0.0;
    static double Batsman2StrikeR = 0.0;

    static int Batsman1six = 0;
    static int Batsman2six = 0;
    static Label RunRateLabel;
    static Label batBoldLabel;
    static Label bat1Label;
    static Label bat2Label;
    static Font boldFont = Font.font("Calibari", FontWeight.BOLD, 24);
    static Font name = Font.font("Times New Roman", 20);

    static Label name1;
    static Label name2;
    static Label batters;

    static int inning1temp = 0; 


    static int Baller1overs = 0;
    static int Baller1runsconceeded = 0;
    static double Baller1economy = 0.0;
    static int Baller1wickets = 0;
    
    private static Label ballBoldLabel;
    private static Label ball1Label;
    private static VBox ballDataVBox;
    static Label BowlerLabel;
    static Label ballname;
    static VBox ballNameVBox;
    static int TotaloversMatch = 0;
    static String[] Team1arr = new String[13];
    static String[] Team2arr = new String[13];

    static String[] for2ndEnningBattingTeam;
    static String[] for2ndEnningBowlingTeam;

    static String team1;
    static String team2;
    static String tosswinner;
    static String Choose;

    static String outBat = "";

    static Label RunRate;
    static Label RequiredRunRate;
    static int target = 0;
    static double remainingOvers = 0.0;
    static int Inning1Wickets;
    static int Inning1overs;
    static int Inning1balls;
    static Label Inning1Score;
    static int Inning1score;
    static int inning1ballstemp;
    static Label Team1Label;
    static Label Team2Label;


    public static void ScoreBoard2(Stage primaryStage,String Team1,String Team2,String TossWinner,String Choosedorder,String Bat1,String Bat2,String Bowler, String[] BattingteamArr, String[] BowlingteamArr,int overs,int Target,int inning1Wickets,int ining1overs,int inning1balls) {

        Batsman1 = Bat1;
        Batsman2 = Bat2;
        BowlerName1 = Bowler;
        TotaloversMatch = overs;
        team1 = Team1;
        team2 = Team2;
        tosswinner = TossWinner;
        Choose = Choosedorder;
        target = Target;
        Inning1overs = ining1overs;
        Inning1balls = inning1balls;
        Inning1Wickets = inning1Wickets;
        Inning1score = Target;
        inning1ballstemp = inning1balls;



        //************************************************************************************************
        Team1arr = BattingteamArr;
        Team2arr = BowlingteamArr;
        for2ndEnningBattingTeam = BowlingteamArr;
        for2ndEnningBowlingTeam = BattingteamArr;


        if(inning1balls == 6){
            Inning1overs++;
            Inning1balls = 0;
        }


        int index1 = 0;
        int index2 = 0;
        for(int i = 0;i<Team1arr.length;i++){
            if(Team1arr[i].equals(Bat1)){
                index1 = i;
            }
            if(Team1arr[i].equals(Bat2)){
                index2 = i;
            }
        }

        String[] newarr = new String[11];
        int k = 0;
        for(int i = 0;i<13;i++){
            if(i != index1 && i!=index2){
                newarr[k] = Team1arr[i];
                k++;
            }
        }
        Team1arr = newarr;
    // *************************************************************************************************************************

        primaryStage.setTitle("Cricket Scoreboard");

        window = primaryStage;
        window.setResizable(true);
        //*************************************************************************************** */


        VBox nameBox = new VBox(20);
        Team1Label = new Label();
        Team2Label = new Label();
        Team1Label.setFont(boldFont);
        Team2Label.setFont(boldFont);
        Team2Label.setText(team1 + "          "+target + " / "+ Inning1Wickets + "( " + Inning1overs + "." + Inning1balls + " )" );
        Team1Label.setText(team2 + "          "+0 + " / "+ 0 + "( " + 0 + "." + 0 + " )");
        Team1Label.setStyle("-fx-font-size:50px; -fx-font-weight:bold; -fx-text-fill: white;");
        Team2Label.setStyle("-fx-font-size:50px; -fx-font-weight:bold; -fx-text-fill: white;");

        nameBox.getChildren().addAll(Team2Label,Team1Label);


        //***************************************************************************************** */

        // Data Entry Section
        Font wickFont = Font.font("Calibri", 25);
        Font buttonFont = Font.font("Calibri", 30);
        Font labelFont = Font.font("Times New Roman", 30);

        dataEntryLabel = new Label("\n***********ENTER RUNS SCORED***********\n\n");
        dataEntryLabel.setFont(labelFont);
        dataEntryLabel.setAlignment(Pos.TOP_CENTER);
        dataEntryLabel.setStyle("-fx-font-size:35px; -fx-font-weight:bold; -fx-text-fill: white;");
        dataEntryLabel.setPadding(new Insets(10, 0, 0, 150));

        dotButton = createRunsButton(" 0 ", 0);
        dotButton.setFont(buttonFont);
        dotButton.setMaxWidth(140);

        oneRunButton = createRunsButton("1", 1);
        oneRunButton.setFont(buttonFont);
        oneRunButton.setMaxWidth(140);
        
        twoRunButton = createRunsButton("2", 2);
        twoRunButton.setFont(buttonFont);
        twoRunButton.setMaxWidth(140);
        
        threeRunButton = createRunsButton("3", 3);
        threeRunButton.setFont(buttonFont);
        threeRunButton.setMaxWidth(140);
        
        fourRunButton = createRunsButton("4", 4);
        fourRunButton.setFont(buttonFont);
        fourRunButton.setMaxWidth(140);
        
        fiveRunButton = createRunsButton("5", 5);
        fiveRunButton.setFont(buttonFont);
        fiveRunButton.setMaxWidth(140);

        sixRunButton = createRunsButton("6", 6);
        sixRunButton.setFont(buttonFont);
        sixRunButton.setMaxWidth(140);


        noBallMenuButton = new MenuButton("NOBALL");
        noBallMenuButton.setFont(wickFont);
        noBallMenuButton.setPrefWidth(150);

        undoButton = new Button("UNDO");
        undoButton.setFont(wickFont);
        undoButton.setPrefWidth(150);

        MenuItem noBall1 = new MenuItem("1");
        noBall1.setStyle("-fx-font-size: 20px;");
        MenuItem noball2 = new MenuItem("2");
        noball2.setStyle("-fx-font-size: 20px;");
        MenuItem noBall3 = new MenuItem("3");
        noBall3.setStyle("-fx-font-size: 20px;");
        MenuItem noball4 = new MenuItem("4");
        noball4.setStyle("-fx-font-size: 20px;");
        MenuItem noBall5 = new MenuItem("5");
        noBall5.setStyle("-fx-font-size: 20px;");
        MenuItem noBall6 = new MenuItem("6");
        noBall5.setStyle("-fx-font-size: 20px;");
        MenuItem noBall7 = new MenuItem("7");
        noBall5.setStyle("-fx-font-size: 20px;");
        // Add menu items to the MenuButton
        
        noBall1.setOnAction(e-> {
            addExtraruns(1);
            
        });

         noball2.setOnAction(e-> {
            addExtraruns(2);
            noballbatsmandataupdate(2-1);

         });
         noBall3.setOnAction(e-> {
            addExtraruns(3);
            noballbatsmandataupdate(3-1);
            
         });
         noball4.setOnAction(e-> {
            addExtraruns(4);
            noballbatsmandataupdate(4-1);
         });
         noBall5.setOnAction(e-> {
            addExtraruns(5);
            noballbatsmandataupdate(5-1);
         });
         noBall6.setOnAction(e-> {
            addExtraruns(6);
            noballbatsmandataupdate(6-1);
         });
         noBall7.setOnAction(e-> {
            addExtraruns(7);
            noballbatsmandataupdate(7-1);
         });
         noBallMenuButton.getItems().addAll(noBall1,noball2,noBall3,noball4,noBall5,noBall6,noBall7);
        
        //Font wideFont = Font.font("Calibri", 20);
        wideMenuButton = new MenuButton("WIDE");
        wideMenuButton.setFont(wickFont);
        wideMenuButton.setPrefWidth(150);

        MenuItem wide1 = new MenuItem("1");
        wide1.setStyle("-fx-font-size: 20px;");
        MenuItem wide2 = new MenuItem("2");
        wide2.setStyle("-fx-font-size: 20px;");
        MenuItem wide3 = new MenuItem("3");
        wide3.setStyle("-fx-font-size: 20px;");
        MenuItem wide4 = new MenuItem("4");
        wide4.setStyle("-fx-font-size: 20px;");
        MenuItem wide5 = new MenuItem("5");
        wide5.setStyle("-fx-font-size: 20px;");
        MenuItem wide6 = new MenuItem("6");
        wide6.setStyle("-fx-font-size: 20px;");

        // Add menu items to the MenuButton
        wideMenuButton.getItems().addAll(wide1,wide2,wide3,wide4,wide5,wide6);

        wide1.setOnAction(e -> {
            addExtraruns(1);
        });
        wide2.setOnAction(e -> {
            addExtraruns(2);
        });
        wide3.setOnAction(e -> {
            addExtraruns(3);
        });
        wide4.setOnAction(e -> {
            addExtraruns(4);
        });
        wide5.setOnAction(e -> {
            addExtraruns(5);
        });
        wide6.setOnAction(e -> {
            addExtraruns(6);
        });

        byesMenuButton = new MenuButton("BYES");
        byesMenuButton.setFont(wickFont);
        byesMenuButton.setPrefWidth(150);

        MenuItem byes1 = new MenuItem("BYES 1");
        byes1.setStyle("-fx-font-size: 20px;");
        MenuItem byes2 = new MenuItem("BYES 2");
        byes2.setStyle("-fx-font-size: 20px;");
        MenuItem byes3 = new MenuItem("BYES 3");
        byes3.setStyle("-fx-font-size: 20px;");
        MenuItem byes4 = new MenuItem("BYES 4");
        byes4.setStyle("-fx-font-size: 20px;");
        MenuItem byes5 = new MenuItem("BYES 5");
        byes5.setStyle("-fx-font-size: 20px;");
        MenuItem byes6 = new MenuItem("BYES 6");
        byes6.setStyle("-fx-font-size: 20px;");
        
        // Add menu items to the MenuButton
      
        byesMenuButton.getItems().addAll(byes1,byes2,byes3,byes4,byes5,byes6);

        byes1.setOnAction(e -> {
            addbyesruns(1);
        });
        byes2.setOnAction(e -> {
            addbyesruns(2);
        });
        byes3.setOnAction(e -> {
            addbyesruns(3);
        });
        byes4.setOnAction(e -> {
            addbyesruns(4);
        });
        byes5.setOnAction(e -> {
            addbyesruns(5);
        });
        byes6.setOnAction(e -> {
            addbyesruns(6);
        });


        wicketMenuButton = new MenuButton("WICKET");
        wicketMenuButton.setFont(wickFont);
        wicketMenuButton.setPrefWidth(150);

        MenuItem lbw = new MenuItem("LBW");
        lbw.setStyle("-fx-font-size: 20px;");
        MenuItem bold = new MenuItem("BOLD");
        bold.setStyle("-fx-font-size: 20px;");
        MenuItem catchout = new MenuItem("CATCH");
        catchout.setStyle("-fx-font-size: 20px;");
        MenuItem runout = new MenuItem("RUNOUT");
        runout.setStyle("-fx-font-size: 20px;");
        MenuItem hit = new MenuItem("HIT");
        hit.setStyle("-fx-font-size: 20px;");
        
        lbw.setOnAction(e->{
            submitWicket(0,"Lbw");


        });
        bold.setOnAction(e->{
            submitWicket(0,"Bold");
        });
        catchout.setOnAction(e->{
            submitWicket(0,"Catch");
        });
        // runout.setOnAction(e->{

        //     submitWicket(runs,"Runout");
        // });
        runout.setOnAction(e -> {
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Runout");

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window");

            Label runoutLabel = new Label("!!! Runout !!!\n\n\n");
            runoutLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 40));
            TextField runsScoredInput = new TextField();
            runsScoredInput.setFont(boldFont);
            runsScoredInput.setMaxWidth(150);
            runsScoredInput.setMinHeight(40);
            runsScoredInput.setAlignment(Pos.CENTER);
            runsScoredInput.setPromptText("Enter runs scored");
        
            Button submitButton = new Button("Submit");
            submitButton.setFont(boldFont);
            submitButton.getStyleClass().add("button");

            submitButton.setOnAction(submitEvent -> {
                    popupStage.close();
                
            });

            Label EnterRuns = new Label("Enter Runs Scored\n\n");
            EnterRuns.setFont(Font.font("Arial", FontWeight.BOLD, 30));
            popupLayout.getChildren().addAll(runoutLabel,EnterRuns, runsScoredInput,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700, 500);
            // Screen screen = Screen.getPrimary();
            // Rectangle2D bounds = screen.getVisualBounds();
            // double xOffset = bounds.getWidth() - popupLayout.getWidth() - 130; // Adjust as needed
            // double yOffset = bounds.getHeight() - popupLayout.getHeight() - 80; // Adjust as needed
            // popupStage.setX(xOffset);
            // popupStage.setY(yOffset);

            popupStage.setScene(popupScene);
            popupStage.showAndWait();
            int runs = Integer.parseInt(runsScoredInput.getText());

            submitWicket(runs,"Runout");
        });

        hit.setOnAction(e->{

            submitWicket(0,"Hit");
        });
        
        // Add menu items to the MenuButton
        wicketMenuButton.getItems().addAll(lbw,bold,catchout,runout,hit);

        strikeMenuButton = new MenuButton("STRIKE");

        strikeMenuButton.setFont(wickFont);
        strikeMenuButton.setPrefWidth(150);

        MenuItem p1 = new MenuItem("Player1");
        p1.setStyle("-fx-font-size: 20px;");
        MenuItem p2 = new MenuItem("Player2");
        p2.setStyle("-fx-font-size: 20px;");

        strikeMenuButton.getItems().addAll(p1,p2);

        runsHBox1 = new HBox(30);
        runsHBox1.setAlignment(Pos.TOP_CENTER);
        runsHBox1.setPadding(new Insets(10,0,0,0));
        runsHBox1.getChildren().addAll(dotButton,oneRunButton,twoRunButton,threeRunButton,fourRunButton,fiveRunButton,sixRunButton);

        runsHBox2 = new HBox(30);
        runsHBox2.setAlignment(Pos.TOP_CENTER);
        runsHBox2.setPadding(new Insets(60,0,0,0));
        runsHBox2.getChildren().addAll(noBallMenuButton,wicketMenuButton,wideMenuButton,byesMenuButton);

        strikeHBox = new HBox(30);
        strikeHBox.setPadding(new Insets(70, 0, 0, 300));
        strikeHBox.getChildren().addAll(strikeMenuButton,undoButton);

        dataEntryLayout = new VBox(10);
        runsHBox1.setPadding(new Insets(10,0,0,0));

        // removed striker menu box for further changes 
        dataEntryLayout.getChildren().addAll(dataEntryLabel,runsHBox1,runsHBox2);

        //*******************************************************************************************************ScoreBoard Layout  */
        
        scoreboardLayout = new VBox(10);
        runsTextArea = new TextArea();
        runsTextArea.setEditable(false);

        runRateTextArea = new TextArea();
        runRateTextArea.setEditable(false);

        player1RunsTextArea = new TextArea();
        player1RunsTextArea.setEditable(false);

        player2RunsTextArea = new TextArea();
        player2RunsTextArea.setEditable(false);
        
        wicketsTextArea = new TextArea();
        wicketsTextArea.setEditable(false);
        
        bowler1InfoTextArea = new TextArea();
        bowler2InfoTextArea = new TextArea();
        
        lastSixBallsTextArea = new TextArea();

        //VBox TeamNameVBox = ScoreCard.TeamDisp2(Team1,Team2,Inning1Wickets,Inning1balls,Inning1overs,Inning1score);
        
        // System.out.println("ining 1 Score == " + target + " inning 1 wickets == " + Inning1Wickets + "inning 1 balls = " + Inning1balls + "inning1overs == " + Inning1overs);
        RunRate = ScoreCard.runrateDisp(0.0);
        RunRate.setStyle("-fx-font-size:20px; -fx-font-weight:bold; -fx-text-fill: white;");
        //Inning1Score = new Label(target + " / "+ inning1Wickets + "( " + ining1overs + "." + inning1balls );

        RequiredRunRate = new Label("Required Run Rate : 0.0 ");   
        RequiredRunRate.setStyle("-fx-font-size:20px; -fx-font-weight:bold; -fx-text-fill: white;");
        

        VBox nameRunVBox = new VBox(20);
        nameRunVBox.getChildren().addAll(nameBox,RunRate,RequiredRunRate);
        //scoreLabel = ScoreCard.DispScore2(0,0,0);
        //scoreLabel.setFont(nameFont);
        //scoreLabel.setStyle("-fx-font-size:50px; -fx-font-weight:bold; -fx-text-fill: white;");


        nameScoreVBox = new HBox();
        nameScoreVBox.setPadding(new Insets(50,0,0,50));
        nameScoreVBox.getChildren().addAll(nameRunVBox);
        
        VBox battersVBox = new VBox(20);


        batters = new Label("Batters");
        batters.setFont(boldFont);
        batters.setStyle("-fx-font-size:24px; -fx-font-weight:bold; -fx-text-fill: white;");
        name1 = new Label(" * "+ Bat1);
        name2 = new Label(Bat2);
        name1.setFont(name);
        name2.setFont(name);
        name1.setStyle("-fx-font-size:20px; -fx-font-weight:bold; -fx-text-fill: white;");
        name2.setStyle("-fx-font-size:20px; -fx-font-weight:bold; -fx-text-fill: white;");

        //nameVBox = new VBox(10);

        battersVBox.getChildren().addAll(batters,name1,name2);



        batBoldLabel = new Label();
        batBoldLabel.setText("  R                  B                   4s                   6s                   SR");
        batBoldLabel.setFont(boldFont);
        batBoldLabel.setStyle("-fx-font-size:24px; -fx-font-weight:bold; -fx-text-fill: white;");

        bat1Label = new Label();
        bat2Label = new Label();
        bat1Label.setFont(name);
        bat2Label.setFont(name); 
        bat1Label.setText("  "+ Batsman1runs +"                        "+ Batsman1balls +"                        "+ Batsman1fours +"                          "+ Batsman1six +"                         "+ Batsman1StrikeR);
        bat2Label.setText("  "+ Batsman2runs +"                        "+ Batsman2balls +"                        "+ Batsman2fours +"                          "+ Batsman2six +"                         "+ Batsman2StrikeR);
        bat1Label.setStyle("-fx-text-fill: white;");
        bat2Label.setStyle("-fx-text-fill: white;");


        dataVBox = new VBox(20);
        dataVBox.getChildren().addAll(batBoldLabel,bat1Label,bat2Label);

        battersBox = new HBox(30);
        battersBox.setPadding(new Insets(50,0,0,50));
        battersBox.getChildren().addAll(battersVBox,dataVBox);



        ballBoldLabel = new Label();
        ballBoldLabel.setText("O                 M                 R                 W                 ECO");
        ballBoldLabel.setFont(boldFont);
        ballBoldLabel.setStyle("-fx-font-size:24px; -fx-font-weight:bold; -fx-text-fill: white;");

        ball1Label = new Label();
        ball1Label.setFont(name);
        ball1Label.setStyle("-fx-text-fill: white;");
        ball1Label.setText(Baller1overs+"                      "+0+"                      "+Baller1runsconceeded+"                          "+Baller1wickets+"                         "+Baller1economy);
        ballDataVBox = new VBox(10);
        ballDataVBox.getChildren().addAll(ballBoldLabel,ball1Label);


        BowlerLabel = new Label("Bowlers");
        BowlerLabel.setFont(boldFont);
        BowlerLabel.setStyle("-fx-font-size:24px; -fx-font-weight:bold; -fx-text-fill: white;");
        
        ballname = new Label(BowlerName1);
        ballname.setFont(name);
        ballname.setStyle("-fx-font-size:20px; -fx-font-weight:bold; -fx-text-fill: white;");
        ballNameVBox = new VBox(10);
        ballNameVBox.getChildren().addAll(BowlerLabel,ballname);
    
        //VBox ballDataVBox1 = ScoreCard.updateBalls(0.0,0,0,0,0.0);
        HBox ballersBox1 = new HBox(70);
        ballersBox1.setPadding(new Insets(50,0,0,50));
        ballersBox1.getChildren().addAll(ballNameVBox,ballDataVBox);



        lastVBox = new VBox(5);
        lastVBox.getChildren().addAll(ballersBox1);

        Image backgroundImage = new Image("Images/v1.jpg");
        BackgroundImage background = new BackgroundImage(backgroundImage,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(950, 1050, false, false, false, false));

        Image backgroundImage1 = new Image("Images/v4.jpg");
        BackgroundImage background1 = new BackgroundImage(backgroundImage1,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(950, 1050, false, false, false, false));

        scoreboardLayout.getChildren().addAll(nameScoreVBox,battersBox,lastVBox);
        
        scoreboardLayout.setBackground(new Background(background));
        dataEntryLayout.setBackground(new Background(background1));
        // **********************************************************************************   Start   ->  Create a SplitPane to arrange the windows side by side

        splitPane = new SplitPane(scoreboardLayout,dataEntryLayout);
        splitPane.setDividerPositions(0.5);     // Adjust the divider position as needed 

        // Create the scene
        Scene scene = new Scene(splitPane, 1920, 1000);
        window.setScene(scene);
        window.setMaximized(true);
        window.show();
       
    }

    public static Button createRunsButton(String text, int runs) {

        Button button = new Button(text);
        button.setOnAction(e -> submitRuns(runs));
         return button;
    }
    public static void addExtraruns(int runs){
        Runs = Runs + runs;
        
        //scoreLabel.setText("\n"+ Runs + " / "+ wickets + "( " + over + "." + balls + " )");
        Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");

        double Overs = over + (balls / 6.0);

        remainingOvers = TotaloversMatch - Overs;
        RequiredRunRate.setText(ScoreCard.calculateRequiredRunRate(target, Runs, remainingOvers));

        String Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
        RunRate.setText(Runrate);
        matchresult();
    }
    public static void addbyesruns(int runs){
        Runs = Runs + runs;
        balls++;
        calculateBowler1Economy();
        Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");
        double Overs = over + (balls / 6.0);

        remainingOvers = TotaloversMatch - Overs;
        RequiredRunRate.setText(ScoreCard.calculateRequiredRunRate(target, Runs, remainingOvers));

        String Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
        RunRate.setText(Runrate);

        if(balls == 6){
            
            over++;
            balls = 0;
            Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
            RunRate.setText(Runrate);

            if(over == TotaloversMatch){
                Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");
                matchresult();
            }
            Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");

            if(over < TotaloversMatch){
                UpdateBaller();
            }
            
        }
        
    }

    public static Button createWicketButton(int runs,String WicketType) {
        Button button = new Button("Wicket");
        button.setOnAction(e -> submitWicket(0,WicketType));
        return button;
    }

    public static void submitRuns(int runs) {
        Runs = Runs + runs;
        balls++;
        double Overs = over + (balls / 6.0);

        remainingOvers = TotaloversMatch - Overs;
        RequiredRunRate.setText(ScoreCard.calculateRequiredRunRate(target, Runs, remainingOvers));
        
        String Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
        RunRate.setText(Runrate);
        Baller1runsconceeded = Baller1runsconceeded + runs;


        //  Start ->  ******************************************************************************************        Add * in front of striker batsman
        if(runs % 2 == 1 &&  balls != 6){
            if(StrikeTemp == 0){
                name2.setText(" * " + Batsman2);
                name1.setText("  " + Batsman1);
            }
            else{
                
                name1.setText( "* " + Batsman1);
                name2.setText(" " + Batsman2);
            }
                
        }  
        else if(runs%2==0 && balls == 6){
            if(StrikeTemp == 0){
                name2.setText("* " + Batsman2);
                name1.setText("  " + Batsman1);
            }
            else{
                
                name1.setText( "* " + Batsman1);
                name2.setText(" " + Batsman2);
            }
        }

        //  End  -> ********************************************************************************************   END
        
        
        // *******************************************************************************************  Start    ->   Change Batsman Data Which is on Strikec After Ball  And update Bowler Data
        

        calculateBowler1Economy();         //   Bowler Economy
        

        if(StrikeTemp == 0){
            

            if(runs == 4){
                Batsman1fours++;
            }
            if(runs == 6){
                Batsman1six++;
            }
            Batsman1runs = Batsman1runs + runs;
            Batsman1balls++;

            double StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
            String str = String.format("%.2f", StrikeRate);

            int Matchid = CreateMatch.matchid();
            BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str,"notout");

            bat1Label.setText("  "+ Batsman1runs +"                        "+ Batsman1balls +"                       "+ Batsman1fours +"                          "+ Batsman1six +"                         "+ str);

            if(runs%2 == 1 &&  balls != 6){
                StrikeTemp = 1;
            }
            if(runs % 2 == 0 && balls == 6){
                StrikeTemp = 1;
            }
           // scoreLabel.setText("\n"+ Runs +" / "+ wickets + "( " + over + "." + balls + " )");
            Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");
            
            matchresult();

        }
        else if(StrikeTemp == 1){
            if(runs == 4){
                Batsman2fours++;
            }
            if(runs == 6){
                Batsman2six++;
            }
            Batsman2runs = Batsman2runs + runs;
            Batsman2balls++;

            double StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
            String str = String.format("%.2f", StrikeRate);

            int Matchid = CreateMatch.matchid();
            BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str,"notout");

            bat2Label.setText("  "+ Batsman2runs +"                        "+ Batsman2balls +"                        "+ Batsman2fours +"                          "+ Batsman2six +"                         "+ str);

            if(runs%2 == 1 &&  balls != 6){
                StrikeTemp = 0;
            }
            
            if(runs % 2 == 0 && balls == 6){
                StrikeTemp = 0;
            }
            //scoreLabel.setText("\n"+ Runs +" / "+ wickets + "( " + over + "." + balls + " )");
            Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");
            matchresult();

        }
        //************************************************************************************************ END 

        // *********************************************************************************************** Start     ->    Change Bowler After Completed an Over
        if(balls == 6){
            
            over++;
            balls = 0;
            Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
            RunRate.setText(Runrate);
            System.out.println("hii");
            System.out.println(over);
            System.out.println(TotaloversMatch);
            if(over == TotaloversMatch){
                //scoreLabel.setText("\n"+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");
                Team1Label.setText(team2 + "            " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");
                matchresult();
            }
           // scoreLabel.setText("\n"+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");
           Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");

            if(over < TotaloversMatch){
                UpdateBaller();
            }
            
        }
        System.out.println(Runs);

        // ************************************************************************************************ END 

        
        // **** ->  Update ScoreBoard 
        
        //scoreLabel.setText("\n"+ Runs +" / "+ wickets + "( " + over + "." + balls + " )");
        Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");

    }

    // ********************************************************************************************   Start ->  Update Wickets Of Batting Team And the Bowler Wickets  And Add New Batsman   

    private static void submitWicket(int runs,String Wickettype) {
        wicketsTextArea.setText("Wickets: W");
        Runs = Runs + runs;
        wickets++;
        

        if(Wickettype.equals("Runout")){
            if(StrikeTemp == 0){
                Batsman1runs = Batsman1runs + runs;
                Batsman1balls++;
                double StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
                String str = String.format("%.2f", StrikeRate);
                
                int Matchid = CreateMatch.matchid();
                BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str,"Run Out");
                bat1Label.setText("  "+ Batsman1runs +"                        "+ Batsman1balls +"                        "+ Batsman1fours +"                          "+ Batsman1six +"                         "+ str);
                
            }
            else{

                Batsman2runs = Batsman2runs + runs;
                Batsman2balls++;
                double StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
                String str = String.format("%.2f", StrikeRate);
                int Matchid = CreateMatch.matchid();
                BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str,"Run Out");

                bat2Label.setText("  "+ Batsman2runs +"                        "+ Batsman2balls +"                        "+ Batsman2fours +"                          "+ Batsman2six +"                         "+ str);
            }
           // scoreLabel.setText("\n"+ Runs +" / "+ wickets + "( " + over + "." + balls + " )");
            Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");
            matchresult();
        }
        else{
            Baller1wickets++;
        }
       
        
        balls++;
       
        double Overs = over + (balls / 6.0);

        remainingOvers = TotaloversMatch - Overs;
        RequiredRunRate.setText(ScoreCard.calculateRequiredRunRate(target, Runs, remainingOvers));

        String Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
        RunRate.setText(Runrate);

        // *   --> update Bowler Economy
        Baller1runsconceeded = Baller1runsconceeded + runs;
        calculateBowler1Economy();

        
        
        
        
        

        //scoreLabel.setText("\n"+ Runs +" / "+ wickets + "( " + over + "." + balls + " )");
        Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");

        Stage SlectPlayerPopup = new Stage();
            SlectPlayerPopup.initModality(Modality.APPLICATION_MODAL);
            SlectPlayerPopup.setTitle("New Batsman");
            

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window"); // Apply the CSS class

            Label AddBatsman = new Label(" Select New Batsman\n\n\n");
            AddBatsman.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));

            ComboBox<String> Team1remainingPlayerComboBox = new ComboBox<>();
            Team1remainingPlayerComboBox.getStyleClass().add("combo-box"); // Apply the ComboBox style
            ComboBox<String> OutPlayer = new ComboBox<>();
            Label SelectOutPlayer = new Label(" Choose The Batsman Out\n\n");
            SelectOutPlayer.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
            
            
            if(Wickettype.equals("Runout")){
                OutPlayer.getItems().add(Batsman1);
                OutPlayer.getItems().add(Batsman2);
            }
            
            else if(StrikeTemp == 0){
                outBat = Batsman1;
            }
            else if(StrikeTemp == 1){
                outBat = Batsman2;
            }


            for(int i = 0;i<Team1arr.length;i++){
                Team1remainingPlayerComboBox.getItems().add(Team1arr[i]);
            }

            Button submitButton = new Button("Submit");
            submitButton.setOnAction(submitEvent -> {
                Team1remainingPlayerComboBox.getValue();
                if(Wickettype.equals("Runout")){    
                    OutPlayer.getValue();
                    if((OutPlayer.getValue()).equals(Batsman1)){

                        Batsman1balls++;
                        double StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
                        String str = String.format("%.2f", StrikeRate);
                        int Matchid = CreateMatch.matchid();
                        BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str,Wickettype + " / "+BowlerName1);

                        Batsman1 = Team1remainingPlayerComboBox.getValue();
                        //****************************************************************************************** 

                        //   Add updates in database

                        // *****************************************************************************************

                        Batsman1StrikeR = 0.0;
                        Batsman1balls = 0;
                        Batsman1fours = 0;
                        Batsman1runs = 0;
                        Batsman1six = 0;

                        StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
                        str = String.format("%.2f", StrikeRate);
                        name1.setText(" " + Batsman1); 
                        bat1Label.setText("  "+ Batsman1runs +"                        "+ Batsman1balls +"                        "+ Batsman1fours +"                         "+ Batsman1six +"                         "+ str);
                        BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str,"Not Out");

                        
                    }
                    else if((OutPlayer.getValue()).equals(Batsman2)){
                        Batsman2balls++;
                        double StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
                        String str = String.format("%.2f", StrikeRate);
                        int Matchid = CreateMatch.matchid();
                        BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str,Wickettype + " / "+BowlerName1);

                        Batsman2 = Team1remainingPlayerComboBox.getValue();
                        Batsman2StrikeR = 0.0;
                        Batsman2balls = 0;
                        Batsman2fours = 0;
                        Batsman2runs = 0;
                        Batsman2six = 0;
                        
                        StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
                        str = String.format("%.2f", StrikeRate);
                        name2.setText(" " + Batsman2);
                        bat2Label.setText("  "+ Batsman2runs +"                        "+ Batsman2balls +"                        "+ Batsman2fours +"                          "+ Batsman2six +"                         "+ str);
                        BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str," Not Out");
                    
                    }
                    
                    
                }

                else{
                    if(outBat.equals(Batsman1)){
                            Batsman1balls++;
                            double StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
                            String str = String.format("%.2f", StrikeRate);
                            int Matchid = CreateMatch.matchid();
                            
                            BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str," Out ");
                            Batsman1 = Team1remainingPlayerComboBox.getValue();
                            //****************************************************************************************** 

                            //   Add updates in database

                            // *****************************************************************************************

                            Batsman1StrikeR = 0.0;
                            Batsman1balls = 0;
                            Batsman1fours = 0;
                            Batsman1runs = 0;
                            Batsman1six = 0;

                            StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
                            str = String.format("%.2f", StrikeRate);
                            Matchid = CreateMatch.matchid();
                            
                            BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str," Not Out ");
                            
                            if(balls != 6){
                                name1.setText("* " + Batsman1);
                            }
                            else{
                                name1.setText(" " + Batsman1);
                                name2.setText("* " + Batsman2);
                            }
                            
                            bat1Label.setText("  "+ Batsman1runs +"                        "+ Batsman1balls +"                        "+ Batsman1fours +"                         "+ Batsman1six +"                         "+ str);
                    }
                    else if(outBat.equals(Batsman2)){
                        Batsman2balls++;
                        double StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
                        String str = String.format("%.2f", StrikeRate);
                        int Matchid = CreateMatch.matchid();
                        BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str,Wickettype + " / "+BowlerName1);
                        
                        Batsman2 = Team1remainingPlayerComboBox.getValue();
                        
                        Batsman2StrikeR = 0.0;
                        Batsman2balls = 0;
                        Batsman2fours = 0;
                        Batsman2runs = 0;
                        Batsman2six = 0;
                        
                        StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
                        str = String.format("%.2f", StrikeRate);
                        BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str," Not Out");

                        if(balls != 6){
                            name2.setText("* " + Batsman2);
                        }
                        else{
                            name1.setText("* " + Batsman1);
                            name2.setText(" " + Batsman2);
                        }
                        bat2Label.setText("  "+ Batsman2runs +"                        "+ Batsman2balls +"                        "+ Batsman2fours +"                          "+ Batsman2six +"                         "+ str);
                    } 
                    
                }
                
                String remainingPlayer[] = new String[Team1arr.length - 1 ];
                int k = 0;
                for(int i = 0;i<Team1arr.length;i++){
                    if(Team1arr[i].equals(Team1remainingPlayerComboBox.getValue()) == false){
                        remainingPlayer[k] = Team1arr[i];
                        k++;
                    }
                }
                Team1arr = remainingPlayer;
                SlectPlayerPopup.close();

                
            });

        if(Wickettype.equals("Runout")){
            popupLayout.getChildren().addAll(SelectOutPlayer,OutPlayer,AddBatsman,Team1remainingPlayerComboBox,submitButton);
        }
        else{
            popupLayout.getChildren().addAll(AddBatsman,Team1remainingPlayerComboBox,submitButton);
        }
        popupLayout.setAlignment(Pos.CENTER);
        popupLayout.getStylesheets().add("styles.css");

        Scene popupScene = new Scene(popupLayout, 700, 500);
        // Screen screen = Screen.getPrimary();

        // Rectangle2D bounds = screen.getVisualBounds();
        // double xOffset = bounds.getWidth() - popupLayout.getWidth() - 130; // Adjust as needed
        // double yOffset = bounds.getHeight() - popupLayout.getHeight() - 80; // Adjust as needed
        // SlectPlayerPopup.setX(xOffset);
        // SlectPlayerPopup.setY(yOffset);

        SlectPlayerPopup.setScene(popupScene);
        SlectPlayerPopup.showAndWait();
        
        if(wickets == 10){
                System.out.println("First Inning Over Start Second Enning ");
                //End1stEnning();
                //scoreLabel.setText("          \n"+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");
                Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");
                matchresult();
        }

        if(balls == 6){
            over++;
            balls = 0;
            if((over == TotaloversMatch) || (wickets == 10)){
                System.out.println("First Inning Over Start Second Enning ");
                //scoreLabel.setText("          \n"+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");
                Team1Label.setText(team2 + "           " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");

                matchresult();

            }
            //scoreLabel.setText("          \n"+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");
            Team1Label.setText(team2 + "            " + Runs + " / "+ wickets + "( " + over + "." + balls + " )");
            if(over < TotaloversMatch && wickets < 10){
                UpdateBaller();
            }
            
        }


    }

    // End  ->  ***********************************************************************************************************************************************   End   

    
    public static VBox updateRuns(int runs, int ball, int four, int six,double SR,int i){
        
        batBoldLabel = new Label();
        batBoldLabel.setText("  R                B                4s                6s                SR");
        batBoldLabel.setFont(boldFont);

        bat1Label = new Label();
        bat2Label = new Label();
        if(i == 0){
            bat1Label.setText("  "+runs+"                     "+ball+"                     "+four+"                       "+six+"                      "+SR);
        }
        else{
            bat2Label.setText("  "+runs+"                     "+ball+"                     "+four+"                       "+six+"                      "+SR);
        }

        dataVBox = new VBox(10);
        dataVBox.getChildren().addAll(batBoldLabel,bat1Label,bat2Label);
        return dataVBox;
    }


    // Start  ->   ***********************************************************************************************     ->   Update Baller When Over Finished  
    public static void UpdateBaller(){
        
        if(over < TotaloversMatch){
            balls = 0;
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Update Bowler");

            VBox popupLayout = new VBox(20);
            popupLayout.getStyleClass().add("popup-window");
            Label OverCompleted = new Label("Over Completed , Choose Bowler!\n\n\n");
            OverCompleted.setFont(Font.font("Arial", FontWeight.BOLD, 20));
            String[] BowlingTeam = Team2arr;

            ComboBox <String> BowlerCombobox = new ComboBox<>();

            for(int i = 0;i<BowlingTeam.length;i++){
                if(BowlingTeam[i].equals(BowlerName1) == false){
                    BowlerCombobox.getItems().add(BowlingTeam[i]);
                }
            }



            Button submitButton = new Button("Submit");
            submitButton.setOnAction(submitEvent -> {

                ballname.setText(BowlerCombobox.getValue());
                BowlerName1   = BowlerCombobox.getValue();
                Baller1economy = 0.0;
                Baller1overs = 0;
                Baller1runsconceeded = 0;
                Baller1wickets = 0;
                
                ball1Label.setText(Baller1overs + "." + balls +"                      "+0+"                      "+Baller1runsconceeded+"                          "+Baller1wickets+"                         "+Baller1economy);

                popupStage.close();
                
            });

            popupLayout.getChildren().addAll(OverCompleted,BowlerCombobox,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700, 500);
            // Screen screen = Screen.getPrimary();
            // Rectangle2D bounds = screen.getVisualBounds();
            // double xOffset = bounds.getWidth() - popupLayout.getWidth() - 130; // Adjust as needed
            // double yOffset = bounds.getHeight() - popupLayout.getHeight() - 80; // Adjust as needed
            // popupStage.setX(xOffset);
            // popupStage.setY(yOffset);

            popupStage.setScene(popupScene);
            popupStage.showAndWait();
        }


    }

    // End ->  **********************************************************************************************************    END   

    //  Start   ->  *******************************************************************************************************************   Baller Economy 
    public static void calculateBowler1Economy() {
        double economy1 = 0.0;
        
        
        double oversBowled = Baller1overs + (balls / 6.0); // Calculate total overs bowled by Bowler
        
        if (oversBowled > 0) {
            double runsConceded = Baller1runsconceeded;
            economy1 =  runsConceded / oversBowled;
        } else {
            economy1 = 0.0;
        }
        ball1Label.setText(Baller1overs + "." + balls +"                      "+0+"                      "+Baller1runsconceeded+"                          "+Baller1wickets+"                         "+economy1);

    }
    
    public static void matchresult(){
        if(wickets == 10 && Runs < target){
            
            System.out.println("Defend Succesfull");
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Defend Succesfull");

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window");
            Label WinLayout = new Label(team1 + "Win By "+ (target - Runs));
            WinLayout.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            


            Button submitButton = new Button(" View Scorecard ");
            submitButton.setOnAction(submitEvent -> {
                    popupStage.close();
                    ViewPlayerList.display(window);
                    //popupStage.close();
                
            });
            
            popupLayout.getChildren().addAll(WinLayout,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700,500);
            popupStage.setScene(popupScene);
            popupStage.showAndWait();
            
        }

        else if(wickets == 10 && Runs == target){
            
            System.out.println("Match Tye");
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Match Tied");

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window");
            Label WinLayout = new Label("Match Tied ");
            WinLayout.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            


            Button submitButton = new Button(" View Scorecard ");
            submitButton.setOnAction(submitEvent -> {
                    popupStage.close();
                    ViewPlayerList.display(window);
                    //popupStage.close();
                
            });
            
            popupLayout.getChildren().addAll(WinLayout,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700,500);
            popupStage.setScene(popupScene);
            popupStage.showAndWait();

        }
        else if(wickets == 10 && Runs > target){
            System.out.println("Chase Succesfull");
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Match Tied");

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window");
            Label WinLayout = new Label(team2 + " Win  ");
            WinLayout.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            


            Button submitButton = new Button(" View Scorecard ");
            submitButton.setOnAction(submitEvent -> {
                    popupStage.close();
                    ViewPlayerList.display(window);
                    //popupStage.close();
                
            });
            
            popupLayout.getChildren().addAll(WinLayout,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700,500);
            popupStage.setScene(popupScene);
            popupStage.showAndWait();
        } 
        else if(over == TotaloversMatch && Runs == target){
            
            System.out.println("Match Tye");
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Match Tied");

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window");
            Label WinLayout = new Label("Match Tied ");
            WinLayout.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            


            Button submitButton = new Button(" View Scorecard ");
            submitButton.setOnAction(submitEvent -> {
                    popupStage.close();
                    ViewPlayerList.display(window);
                    //popupStage.close();
                
            });
            
            popupLayout.getChildren().addAll(WinLayout,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700,500);
            popupStage.setScene(popupScene);
            popupStage.showAndWait();

        }
        else if(over == TotaloversMatch && Runs > target){
            System.out.println("Chase Succesfull");
            
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Chase Succesfull");

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window");
            Label WinLayout = new Label(team2 +  " Win By "+ (10-wickets)+ " Wickets");
            WinLayout.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            


            Button submitButton = new Button(" View Scorecard ");
            submitButton.setOnAction(submitEvent -> {
                    popupStage.close();
                    ViewPlayerList.display(window);
                    //popupStage.close();
                
            });
            
            popupLayout.getChildren().addAll(WinLayout,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700,500);
            popupStage.setScene(popupScene);
            popupStage.showAndWait();
        }
        else if(Runs > target){
            System.out.println("Chase Succesfull");

            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Chase Succesfull");

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window");
            Label WinLayout = new Label(team2 +  " Win By "+ (10-wickets)+ " Wickets");
            WinLayout.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            


            Button submitButton = new Button(" View Scorecard ");
            submitButton.setOnAction(submitEvent -> {
                    popupStage.close();
                    ViewPlayerList.display(window);
                    //popupStage.close();
                
            });
            
            popupLayout.getChildren().addAll(WinLayout,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700,500);
            popupStage.setScene(popupScene);
            popupStage.showAndWait();
        }
        else if(over == TotaloversMatch && Runs < target){
            
            System.out.println("Defend Succesfull");
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Defend Succesfull");

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window");
            Label WinLayout = new Label(team1 +  " Win By "+ (target-Runs) + " Runs ");
            WinLayout.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            


            Button submitButton = new Button(" View Scorecard ");
            submitButton.setOnAction(submitEvent -> {
                    popupStage.close();
                    ViewPlayerList.display(window);
                    
                
            });
            
            popupLayout.getChildren().addAll(WinLayout,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700,500);
            popupStage.setScene(popupScene);
            popupStage.showAndWait();
        }
        else{
            System.out.println("No Result");
        }

    }
    public static void noballbatsmandataupdate(int runs){
        
        if(runs % 2 == 1 &&  balls != 6){
            if(StrikeTemp == 0){
                name2.setText(" * " + Batsman2);
                name1.setText("  " + Batsman1);
            }
            else{
                
                name1.setText( "* " + Batsman1);
                name2.setText(" " + Batsman2);
            }
                
        }  
        else if(runs%2==0 && balls == 6){
            if(StrikeTemp == 0){
                name2.setText("* " + Batsman2);
                name1.setText("  " + Batsman1);
            }
            else{
                
                name1.setText( "* " + Batsman1);
                name2.setText(" " + Batsman2);
            }
        }

        if(StrikeTemp == 0){
            

            if(runs == 4){
                Batsman1fours++;
            }
            if(runs == 6){
                Batsman1six++;
            }
            Batsman1runs = Batsman1runs + runs;
            Batsman1balls++;
            double StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
            String str = String.format("%.2f", StrikeRate);
            int Matchid = CreateMatch.matchid();
            BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str,"notout");
            
            // int Match_id = CreateMatch.matchid();
            // BattingDataUpdater.updateBattingData(Match_id,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six);
            bat1Label.setText("  "+ Batsman1runs +"                       "+ Batsman1balls +"                       "+ Batsman1fours +"                         "+ Batsman1six +"                        "+ str);

            if(runs%2 == 1 &&  balls != 6){
                StrikeTemp = 1;
            }
            if(runs % 2 == 0 && balls == 6){
                StrikeTemp = 1;
            }

        }
        else if(StrikeTemp == 1){
            if(runs == 4){
                Batsman2fours++;
            }
            if(runs == 6){
                Batsman2six++;
            }
            Batsman2runs = Batsman2runs + runs;
            Batsman2balls++;

            double StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
            String str = String.format("%.2f", StrikeRate);

            int Matchid = CreateMatch.matchid();
            BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str,"notout");

            bat2Label.setText("  "+ Batsman2runs +"                       "+ Batsman2balls +"                       "+ Batsman2fours +"                         "+ Batsman2six +"                        "+ str);

            if(runs%2 == 1 &&  balls != 6){
                StrikeTemp = 0;
            }
            
            if(runs % 2 == 0 && balls == 6){
                StrikeTemp = 0;
            }

        }
    }
    
}

