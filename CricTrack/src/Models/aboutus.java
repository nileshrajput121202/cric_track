package Models;

import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.control.Label;

public class aboutus{

    public static void about() {
        Stage popupStage = new Stage();
        GridPane root = new GridPane();
root.setMinHeight(Region.USE_PREF_SIZE);
root.setMinWidth(Region.USE_PREF_SIZE);
root.setPrefHeight(900.0);
root.setPrefWidth(1600.0);

ImageView backgroundImageView = new ImageView(new Image("Images/a2.jpg"));
backgroundImageView.setFitWidth(1600);
backgroundImageView.setFitHeight(900);
backgroundImageView.setPreserveRatio(false);

Pane imagePane = new Pane(backgroundImageView);
imagePane.setPrefWidth(1600);
imagePane.setPrefHeight(900);
root.getChildren().add(imagePane);

Pane topPane = new Pane();
topPane.setPrefHeight(131.0);
topPane.setPrefWidth(1600.0);
topPane.setStyle("-fx-background-color: #6495ED;");
GridPane.setColumnSpan(topPane, 3);


        ImageView logoImageView = new ImageView(new Image("file:CricTrack_2/crictrack/CricTrack1.0/src/Images/thankyou.jpg"));
        logoImageView.setFitHeight(73.0);
        logoImageView.setFitWidth(351.0);
        logoImageView.setLayoutX(9.0);
        logoImageView.setLayoutY(29.0);

        Label titleLabel = new Label("CrickTrack");
        titleLabel.setLayoutX(470.0);
        titleLabel.setLayoutY(23.0);
        titleLabel.setPrefHeight(86.0);
        titleLabel.setPrefWidth(649.0);
        titleLabel.setTextFill(javafx.scene.paint.Color.WHITE);
        titleLabel.setFont(new Font("System Bold", 59.0));

        ImageView reconImageView = new ImageView(new Image("file:///../../IMAGES/WhatsApp%20Image%202023-10-03%20at%2015.53.35_97cb7b45.jpg"));
        reconImageView.setFitHeight(90.0);
        reconImageView.setFitWidth(90.0);
        reconImageView.setLayoutX(1302.0);
        reconImageView.setLayoutY(18.0);

        Label reconLabel = new Label("Byte");
        reconLabel.setLayoutX(1406.0);
        reconLabel.setLayoutY(28.0);
        reconLabel.setTextFill(javafx.scene.paint.Color.WHITE);
        reconLabel.setFont(new Font("Berlin Sans FB Demi Bold", 31.0));

        Label raptorsLabel = new Label("Busters");
        raptorsLabel.setLayoutX(1409.0);
        raptorsLabel.setLayoutY(58.0);
        raptorsLabel.setTextFill(javafx.scene.paint.Color.WHITE);
        raptorsLabel.setFont(new Font("Berlin Sans FB Demi Bold", 31.0));

        topPane.getChildren().addAll(logoImageView, titleLabel, reconImageView, reconLabel, raptorsLabel);

        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setPrefHeight(200.0);
        anchorPane.setPrefWidth(200.0);
        GridPane.setRowIndex(anchorPane, 1);

        ImageView backgroundImageImageView = new ImageView(new Image("file:///../../IMAGES/Blueback.jpg"));
        backgroundImageImageView.setFitHeight(771.0);
        backgroundImageImageView.setFitWidth(1600.0);
        backgroundImageImageView.setLayoutX(3.0);
        backgroundImageImageView.setLayoutY(1.0);

        ImageView thankYouImageView = new ImageView(new Image("Images\\shashi11c.jpg"));
        thankYouImageView.setFitHeight(300.0);
        thankYouImageView.setFitWidth(300.0);
        thankYouImageView.setLayoutX(180.0);
        thankYouImageView.setLayoutY(50.0);

        ImageView thankYouImageAbove = new ImageView(new Image("file:CricTrack_2/crictrack/CricTrack1.0/src/Images/thankyou.jpg"));
        thankYouImageAbove.setFitHeight(364.0);
        thankYouImageAbove.setFitWidth(569.0);
        thankYouImageAbove.setLayoutX(50.0);
        thankYouImageAbove.setLayoutY(28.0);

        VBox thankYouBox = new VBox();
        thankYouBox.setAlignment(javafx.geometry.Pos.CENTER);
        thankYouBox.setLayoutX(684.0);
        thankYouBox.setLayoutY(-10.0);
        thankYouBox.setPrefHeight(401.0);
        thankYouBox.setPrefWidth(885.0);

        Label thankYouLabel1 = new Label("We wanted to take a moment to express our heartfelt");
        thankYouLabel1.setTextFill(javafx.scene.paint.Color.BLACK);
        thankYouLabel1.setFont(new Font(34.0));

        Label thankYouLabel2 = new Label("gratitude for the invaluable guidance you have provided.");
        thankYouLabel2.setTextFill(javafx.scene.paint.Color.BLACK);
        thankYouLabel2.setFont(new Font(34.0));

        Label thankYouLabel3 = new Label("Your expertise and support have been instrumental in");
        thankYouLabel3.setTextFill(javafx.scene.paint.Color.BLACK);
        thankYouLabel3.setFont(new Font(34.0));

        Label thankYouLabel4 = new Label("helping us navigate through various challenges and");
        thankYouLabel4.setTextFill(javafx.scene.paint.Color.BLACK);
        thankYouLabel4.setFont(new Font(34.0));

        Label thankYouLabel5 = new Label("achieve my goals. We truly appreciate the time and effort");
        thankYouLabel5.setTextFill(javafx.scene.paint.Color.BLACK);
        thankYouLabel5.setFont(new Font(34.0));

        Label thankYouLabel6 = new Label("you've invested in our growth and development.");
        thankYouLabel6.setTextFill(javafx.scene.paint.Color.BLACK);
        thankYouLabel6.setFont(new Font(34.0));

        Text namesText = new Text("Our Team");
        namesText.setFill(javafx.scene.paint.Color.BLACK);
        namesText.setFont(new Font(48.0));
        namesText.setLayoutX(229.0);
        namesText.setLayoutY(426.0);
        namesText.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        namesText.setStrokeWidth(0.0);
        namesText.setWrappingWidth(237.19149780273438);

        Text nameText1 = new Text("Kailas Dukare");
        nameText1.setFill(javafx.scene.paint.Color.BLACK);
        nameText1.setFont(new Font(35.0));
        nameText1.setLayoutX(248.0);
        nameText1.setLayoutY(485.0);
        nameText1.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        nameText1.setStrokeWidth(0.0);

        Text nameText2 = new Text("Nilesh Rajput");
        nameText2.setFill(javafx.scene.paint.Color.BLACK);
        nameText2.setFont(new Font(35.0));
        nameText2.setLayoutX(248.0);
        nameText2.setLayoutY(537.0);
        nameText2.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        nameText2.setStrokeWidth(0.0);

        Text nameText3 = new Text("Vaibhav Gawali");
        nameText3.setFill(javafx.scene.paint.Color.BLACK);
        nameText3.setFont(new Font(35.0));
        nameText3.setLayoutX(247.0);
        nameText3.setLayoutY(596.0);
        nameText3.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        nameText3.setStrokeWidth(0.0);

        Text nameText4 = new Text("Payal Gaikawad");
        nameText4.setFill(javafx.scene.paint.Color.BLACK);
        nameText4.setFont(new Font(35.0));
        nameText4.setLayoutX(245.0);
        nameText4.setLayoutY(652.0);
        nameText4.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        nameText4.setStrokeWidth(0.0);
        
        Text nameText5 = new Text("Srushti Uppar");
        nameText5.setFill(javafx.scene.paint.Color.BLACK);
        nameText5.setFont(new Font(35.0));
        nameText5.setLayoutX(245.0);
        nameText5.setLayoutY(702.0); // Adjusted the layoutY to position it below Payal Gaikawad
        nameText5.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        nameText5.setStrokeWidth(0.0);

        Text thankYouText = new Text("THANK YOU!");
        thankYouText.setFill(Color.BLACK);
        thankYouText.setFont(Font.font(69.0));
        thankYouText.setLayoutX(873.0);
        thankYouText.setLayoutY(534.0);

         ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(1.0), thankYouText);
        scaleTransition.setFromX(0.5);
        scaleTransition.setFromY(0.5);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);

        // Create a FadeTransition to make the text appear
        FadeTransition fadeTransition = new FadeTransition(Duration.seconds(1.0), thankYouText);
        fadeTransition.setFromValue(0.0);
        fadeTransition.setToValue(1.0);

       
        thankYouBox.getChildren().addAll(thankYouLabel1, thankYouLabel2, thankYouLabel3, thankYouLabel4, thankYouLabel5, thankYouLabel6);
        anchorPane.getChildren().addAll(backgroundImageImageView, thankYouImageView, thankYouImageAbove, thankYouBox, namesText, nameText1, nameText2, nameText3, nameText4, nameText5, thankYouText);

        root.getChildren().addAll(topPane, anchorPane);
        Scene scene = new Scene(root);
        popupStage.setScene(scene);
        scaleTransition.play();
        fadeTransition.play();
        popupStage.showAndWait();
    }
}
