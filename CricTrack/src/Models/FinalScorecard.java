// package Models;
// import javafx.application.Application;
// import javafx.scene.Scene;
// import javafx.scene.control.SplitPane;
// import javafx.scene.control.TableColumn;
// import javafx.scene.control.TableView;
// import javafx.scene.control.cell.PropertyValueFactory;
// import javafx.scene.layout.StackPane;
// import javafx.stage.Stage;

// public class FinalScorecard extends Application {
//     @Override
//     public void start(Stage primaryStage) {
//         // Create SplitPanes
//         SplitPane firstInningSplitPane = new SplitPane();
//         SplitPane secondInningSplitPane = new SplitPane();


//         // Create tables for batting and bowling data
//         TableView<PlayerList> firstInningBattingTable = createTable();
//         TableView<PlayerList> firstInningBowlingTable = createTable();
//         TableView<PlayerList> secondInningBattingTable = createTable();
//         TableView<PlayerList> secondInningBowlingTable = createTable();

//         // Add tables to SplitPanes
//         firstInningSplitPane.getItems().addAll(firstInningBattingTable, firstInningBowlingTable);
//         secondInningSplitPane.getItems().addAll(secondInningBattingTable, secondInningBowlingTable);

//         // Sample data for batting and bowling stats
//         PlayerList player1 = new PlayerStats("Player 1", 50, 100, 10);
//         PlayerList player2 = new PlayerStats("Player 2", 30, 90, 8);
//         PlayerList bowler1 = new PlayerStats("Bowler 1", 0, 24, 2);
//         PlayerList bowler2 = new PlayerStats("Bowler 2", 1, 30, 3);

//         firstInningBattingTable.getItems().addAll(player1, player2);
//         firstInningBowlingTable.getItems().addAll(bowler1, bowler2);
//         secondInningBattingTable.getItems().addAll(player1, player2);
//         secondInningBowlingTable.getItems().addAll(bowler1, bowler2);

//         // Create a root StackPane to hold the SplitPanes
//         StackPane root = new StackPane();
//         root.getChildren().addAll(firstInningSplitPane, secondInningSplitPane);

//         Scene scene = new Scene(root, 800, 600);
//         scene.getStylesheets().add("styles.css"); // Load your custom stylesheet

//         primaryStage.setTitle("Cricket Scorecard");
//         primaryStage.setScene(scene);
//         primaryStage.show();
//     }

//     // ...

//     private TableView<PlayerStats> createTable() {
//         TableView<PlayerStats> table = new TableView<>();

//         TableColumn<PlayerStats, String> playerNameCol = new TableColumn<>("Player Name");
//         playerNameCol.setCellValueFactory(new PropertyValueFactory<>("playerName"));

//         TableColumn<PlayerStats, Integer> runsCol = new TableColumn<>("Runs");
//         runsCol.setCellValueFactory(new PropertyValueFactory<>("runs"));

//         TableColumn<PlayerStats, Integer> ballsCol = new TableColumn<>("Balls");
//         ballsCol.setCellValueFactory(new PropertyValueFactory<>("balls"));

//         TableColumn<PlayerStats, Integer> strikeRateCol = new TableColumn<>("Strike Rate");
//         strikeRateCol.setCellValueFactory(new PropertyValueFactory<>("strikeRate"));

//         table.getColumns().addAll(playerNameCol, runsCol, ballsCol, strikeRateCol);

//         return table;
//     }

//     // ...

//     public static void main(String[] args) {
//         launch(args);
//     }
// }

