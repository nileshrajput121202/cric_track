

package Models;

public class PlayerList {

    private String playerName; 
    private String teamName; 
    private int runScored; 
    private int ballsFaced; 
    private int sixes;
    private int fours;
    private String strikeRate; 
    private String dismissalType1; 
    private int wickets;
    private double oversBowled;
    private String economy;
    private int runsConceded;
    private int matchId; 

        PlayerList(String playerName, String teamName, int runScored, int ballsFaced, int sixes, int fours, String strikeRate, String dismissalType, int wickets, double oversBowled, String economy, int runsConceded, int matchId) {
        this.playerName = playerName;
        this.teamName = teamName;
        this.runScored = runScored;
        this.ballsFaced = ballsFaced;
        this.sixes = sixes;
        this.fours = fours;
        this.strikeRate = strikeRate;
        this.dismissalType1 = dismissalType;
        this.wickets = wickets;
        this.oversBowled = oversBowled;
        this.economy = economy;
        this.runsConceded = runsConceded;
        this.matchId = matchId;
    }
        PlayerList(String playerName,int runScored, int ballsFaced, int sixes, int fours, String strikeRate, String dismissalType) {
        this.playerName = playerName;
        this.runScored = runScored;
        this.ballsFaced = ballsFaced;
        this.sixes = sixes;
        this.fours = fours;
        this.strikeRate = strikeRate;
        this.dismissalType1 = dismissalType;
    }
        PlayerList(String Playername,int wickets, double oversBowled, String economy, int runsConceded){
        this.wickets = wickets;
        this.oversBowled = oversBowled;
        this.economy = economy;
        this.runsConceded = runsConceded;
    }

   
    public String getPlayerName() {
        return playerName;
    }

    public String getTeamName() {
        return teamName;
    }

    public int getRunScored() {
        return runScored;
    }

    public int getBallsFaced() {
        return ballsFaced;
    }

    public int getSixes() {
        return sixes;
    }

    public int getFours() {
        return fours;
    }

    public String getStrikeRate() {
        return strikeRate;
    }

    public String getDismissalType() {
        return dismissalType1;
    }

    public int getWickets() {
        return wickets;
    }

    public double getOversBowled() {
        return oversBowled;
    }

    public String getEconomy() {
        return economy;
    }

    public int getRunsConceded() {
        return runsConceded;
    }

    public int getMatchId() {
        return matchId;
    }
    
}
