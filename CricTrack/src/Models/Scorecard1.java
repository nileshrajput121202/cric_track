package Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;


public class Scorecard1 {

    public static ObservableList<PlayerList> Playerslist = FXCollections.observableArrayList();
    public static ObservableList<PlayerList> Inning1teamlist = FXCollections.observableArrayList();
    public static ObservableList<PlayerList> Inning2teamlist = FXCollections.observableArrayList();

    public static ObservableList<PlayerList> Inning1battinglist = FXCollections.observableArrayList();
    public static ObservableList<PlayerList> Inning1Bowlinglist = FXCollections.observableArrayList();
    public static ObservableList<PlayerList> Inning2Battinglist = FXCollections.observableArrayList();
    public static ObservableList<PlayerList> Inning2Bowlinglist = FXCollections.observableArrayList();

    // Get player details from the database
    public static void clearl(){
        Playerslist.clear();
        Inning1battinglist.clear();
        Inning1Bowlinglist.clear();
        Inning2Battinglist.clear();
        Inning2Bowlinglist.clear();
    }
    public static ObservableList<PlayerList> getPlayersDetailsList() {
        //ObservableList<PlayerList> plist = FXCollections.observableArrayList();

        String dbUrl = "jdbc:mysql://localhost:3306/crictrack";
        String dbUser = "root";
        String dbPassword = "Pass@123";

        

        try (Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword)) {
            String selectQuery = "SELECT * FROM players";

            try (PreparedStatement preparedStatement = connection.prepareStatement(selectQuery)) {
                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    // Retrieve player information from the database
                    String Playername = resultSet.getString("PlayerName");
                    System.out.println(Playername);
                    String Teamname = resultSet.getString("TeamName");
                     System.out.println(Teamname);
                    int Playeruns = resultSet.getInt("RunScored");
                    int Ballsfaced = resultSet.getInt("BallsFaced");
                    int sixes = resultSet.getInt("Sixes");
                    int fours = resultSet.getInt("Fours");
                    String StrikeRates = resultSet.getString("StrikeRate");
                    String DismisalType = resultSet.getString("DismisalType");
                    int Wickets = resultSet.getInt("Wickets");
                    Double OversBowled = resultSet.getDouble("OversBowled");
                    String Economy = resultSet.getString("Economy");
                    int RunsConceeded = resultSet.getInt("RunsConceeded");
                    int matchid = resultSet.getInt("Match_id");

                    

                    // Check if the player's match ID matches the desired match ID (CreateMatch.matchid())
                    // if (CreateMatch.matchid() == matchid) {
                        PlayerList player = new PlayerList(Playername, Teamname, Playeruns, Ballsfaced, sixes, fours, StrikeRates, DismisalType, Wickets, OversBowled, Economy, RunsConceeded,matchid);
                        Playerslist.add(player);
                //     }
                //     if(CreateMatch.matchid() == matchid && CreateMatch.BattingTeamname().equals(Teamname)){
                //         PlayerList batdata1 = new PlayerList(Playername, Playeruns,Ballsfaced, sixes, fours, StrikeRates,DismisalType);
                //         Inning1battinglist.add(batdata1);
                    
                //     }
                //     if(CreateMatch.matchid() == matchid && CreateMatch.BowlingTeamname().equals(Teamname)){
                //         PlayerList batdata2 = new PlayerList(Playername, Playeruns, Ballsfaced, sixes, fours, StrikeRates,DismisalType);
                //         Inning2Battinglist.add(batdata2);
                //     }
                //     if(CreateMatch.matchid() == matchid && CreateMatch.BowlingTeamname().equals(Teamname)){
                //         PlayerList balldata1 = new PlayerList(Playername,Wickets, matchid, Economy, RunsConceeded);
                //         Inning1Bowlinglist.add(balldata1);
                //     }
                //     if(CreateMatch.matchid() == matchid && CreateMatch.BattingTeamname().equals(Teamname)){
                //         PlayerList balldata2 = new PlayerList(Playername,Wickets, matchid, Economy, RunsConceeded);
                //         Inning2Bowlinglist.add(balldata2);
                //     }

                 }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        // if(no == 1){
            return Playerslist;
        // }
        // else if(no == 2)
        //     return Inning1battinglist;
        // else if(no == 3)
        //     return Inning2Battinglist;
        // else if(no == 4)
        //     return Inning1Bowlinglist;
        // else if(no == 5)
        //     return Inning2Bowlinglist;
        // else {
        //     return Playerslist;
        // }


        
        

    }

    public static ObservableList<PlayerList> getTeam1List() {
        Playerslist.clear();
        getPlayersDetailsList();
        List<PlayerList> team1List =  Playerslist.stream().filter(plyerObj ->{
            return  (plyerObj.getTeamName().equals(CreateMatch.BattingTeamname()) && (plyerObj.getMatchId() == CreateMatch.matchid()));
        }).collect(Collectors.toList());
        
        Inning1teamlist = FXCollections.observableArrayList(team1List);

        System.out.println(Inning1teamlist + " kailas ");

        return Inning1teamlist;
    }

    public static ObservableList<PlayerList> getTeam2List() {
        Playerslist.clear();
        getPlayersDetailsList();
        List<PlayerList> team2List =  Playerslist.stream().filter(plyerObj ->{
            return  plyerObj.getTeamName().equals(CreateMatch.BowlingTeamName) && (plyerObj.getMatchId() == CreateMatch.matchid());
        }).collect(Collectors.toList());
        
        Inning2teamlist = FXCollections.observableArrayList(team2List);

        System.out.println(Inning2teamlist + "raj ");

        return Inning2teamlist;
    }



    

}


