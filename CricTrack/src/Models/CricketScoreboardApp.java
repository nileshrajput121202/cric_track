package Models;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;



public class CricketScoreboardApp{
    static Label dataEntryLabel;
    static TextField runsInput;
    static Button submitButton;
    //private static Font nameFont = Font.font("Verdana", FontWeight.BOLD, 70);
    static Label scoreboardLabel;
    static TextArea scoreboardTextArea;
    static Stage window;

    static Button oneRunButton;
    static Button dotButton;
    static Button twoRunButton;
    static Button undoButton;
    static Button threeRunButton;
    static Button fourRunButton;
    static Button fiveRunButton;
    static Button sixRunButton;
    static Button noBallButton;
    static Button wicketButton;    
    static Button byesBallButton;
    static MenuButton wideMenuButton;
    static MenuButton byesMenuButton;
    static MenuButton wicketMenuButton;
    static MenuButton noBallMenuButton;
    static MenuButton strikeMenuButton;

    static TextArea runsTextArea;
    static TextArea player1RunsTextArea;
    static TextArea player2RunsTextArea;
    static TextArea runRateTextArea;
    static TextArea wicketsTextArea;
    static TextArea bowler1InfoTextArea;
    static TextArea bowler2InfoTextArea;
    static TextArea lastSixBallsTextArea;
    static VBox scoreboardLayout;
    static SplitPane splitPane;
    static VBox dataEntryLayout;
    static HBox nameScoreVBox;
    static HBox battersBox;
    static VBox lastVBox;
    static VBox dataVBox;
    static HBox runsHBox1;
    static HBox runsHBox2;
    static HBox strikeHBox;
    static Scene scene;
    static Label scoreLabel;
    static int wickets = 0;
    static int over = 0;
    static int balls = 0;
    static String Batsman1 = "";
    static String Batsman2 = "";
    static int StrikeTemp = 0 ;
    static int BowlerTemp = 0;
    static String BowlerName1 = "";
    static String BowlerName2 = "";
    static int Runs= 0;
    static int Batsman1runs  = 0;
    static int Batsman2runs  = 0;
    static int Batsman1balls  = 0;
    static int Batsman2balls  = 0;

    static int Batsman1fours = 0;
    static int Batsman2fours = 0;

    static double Batsman1StrikeR = 0.0;
    static double Batsman2StrikeR = 0.0;

    static int Batsman1six = 0;
    static int Batsman2six = 0;
    static Label RunRateLabel;
    static Label batBoldLabel;
    static Label bat1Label;
    static Label bat2Label;
    static Font boldFont = Font.font("Calibari", FontWeight.BOLD, 24);
    static Font name = Font.font("Times New Roman", 20);
    
    static Label name1;
    static Label name2;
    static Label batters;

    static int inning1temp = 0; 


    static int Baller1overs = 0;
    static int Baller1runsconceeded = 0;
    static double Baller1economy = 0.0;
    static int Baller1wickets = 0;
    
    private static Label ballBoldLabel;
    private static Label ball1Label;
    private static VBox ballDataVBox;
    static Label BowlerLabel;
    static Label ballname;
    static VBox ballNameVBox;
    static int TotaloversMatch = 0;
    static String[] Team1arr = new String[13];
    static String[] Team2arr = new String[13];

    static String[] for2ndEnningBattingTeam;
    static String[] for2ndEnningBowlingTeam;

    static String team1;
    static String team2;
    static String tosswinner;
    static String Choose;

    static String outBat = "";

    static Label RunRate;

    public static void ScoreBoard(Stage primaryStage,String Team1,String Team2,String TossWinner,String Choosedorder,String Bat1,String Bat2,String Bowler, String[] BattingteamArr, String[] BowlingteamArr,int overs) {

        Batsman1 = Bat1;
        Batsman2 = Bat2;
        BowlerName1 = Bowler;
        TotaloversMatch = overs;
        team1 = Team1;
        team2 = Team2;
        tosswinner = TossWinner;
        Choose = Choosedorder;



        //************************************************************************************************
        Team1arr = BattingteamArr;
        Team2arr = BowlingteamArr;
        for2ndEnningBattingTeam = BowlingteamArr;
        for2ndEnningBowlingTeam = BattingteamArr;


        int index1 = 0;
        int index2 = 0;
        for(int i = 0;i<Team1arr.length;i++){
            if(Team1arr[i].equals(Bat1)){
                index1 = i;
            }
            if(Team1arr[i].equals(Bat2)){
                index2 = i;
            }
        }

        String[] newarr = new String[11];
        int k = 0;
        for(int i = 0;i<13;i++){
            if(i != index1 && i!=index2){
                newarr[k] = Team1arr[i];
                k++;
            }
        }
        Team1arr = newarr;
    // *************************************************************************************************************************

        primaryStage.setTitle("Cricket Scoreboard");

        window = primaryStage;
        window.setResizable(true);
        

        // *******************************************************************************************************   Data Entry Section
        Font wickFont = Font.font("Calibri", 25);
        Font buttonFont = Font.font("Calibri", 30);
        Font labelFont = Font.font("Times New Roman", 30);

        dataEntryLabel = new Label("\n***********ENTER RUNS SCORED***********\n\n");
        dataEntryLabel.setFont(labelFont);
        dataEntryLabel.setAlignment(Pos.TOP_CENTER);
        dataEntryLabel.setStyle("-fx-font-size:35px; -fx-font-weight:bold; -fx-text-fill: white;");
        dataEntryLabel.setPadding(new Insets(10, 0, 0, 150));

        dotButton = createRunsButton(" 0 ", 0);
        dotButton.setFont(buttonFont);
        dotButton.setMaxWidth(140);

        oneRunButton = createRunsButton("1", 1);
        oneRunButton.setFont(buttonFont);
        oneRunButton.setMaxWidth(140);
        
        twoRunButton = createRunsButton("2", 2);
        twoRunButton.setFont(buttonFont);
        twoRunButton.setMaxWidth(140);
        
        threeRunButton = createRunsButton("3", 3);
        threeRunButton.setFont(buttonFont);
        threeRunButton.setMaxWidth(140);
        
        fourRunButton = createRunsButton("4", 4);
        fourRunButton.setFont(buttonFont);
        fourRunButton.setMaxWidth(140);
        
        fiveRunButton = createRunsButton("5", 5);
        fiveRunButton.setFont(buttonFont);
        fiveRunButton.setMaxWidth(140);

        sixRunButton = createRunsButton("6", 6);
        sixRunButton.setFont(buttonFont);
        sixRunButton.setMaxWidth(140);


        noBallMenuButton = new MenuButton("NOBALL");
        noBallMenuButton.setFont(wickFont);
        noBallMenuButton.setPrefWidth(150);

        undoButton = new Button("UNDO");
        undoButton.setFont(wickFont);
        undoButton.setPrefWidth(150);

        MenuItem noBall1 = new MenuItem("1");
        noBall1.setStyle("-fx-font-size: 20px;");
        MenuItem noball2 = new MenuItem("2");
        noball2.setStyle("-fx-font-size: 20px;");
        MenuItem noBall3 = new MenuItem("3");
        noBall3.setStyle("-fx-font-size: 20px;");
        MenuItem noball4 = new MenuItem("4");
        noball4.setStyle("-fx-font-size: 20px;");
        MenuItem noBall5 = new MenuItem("5");
        noBall5.setStyle("-fx-font-size: 20px;");
        MenuItem noBall6 = new MenuItem("6");
        noBall6.setStyle("-fx-font-size: 20px;");
        MenuItem noBall7 = new MenuItem("7");
        noBall7.setStyle("-fx-font-size: 20px;");
        // Add menu items to the MenuButton
        
        noBall1.setOnAction(e-> {
            addExtraruns(1);
            
        });

         noball2.setOnAction(e-> {
            addExtraruns(2);
            noballbatsmandataupdate(2-1);

         });
         noBall3.setOnAction(e-> {
            addExtraruns(3);
            noballbatsmandataupdate(3-1);
            
         });
         noball4.setOnAction(e-> {
            addExtraruns(4);
            noballbatsmandataupdate(4-1);
         });
         noBall5.setOnAction(e-> {
            addExtraruns(5);
            noballbatsmandataupdate(5-1);
         });
         noBall6.setOnAction(e-> {
            addExtraruns(6);
            noballbatsmandataupdate(6-1);
         });
         noBall7.setOnAction(e-> {
            addExtraruns(7);
            noballbatsmandataupdate(7-1);
         });

         noBallMenuButton.getItems().addAll(noBall1,noball2,noBall3,noball4,noBall5,noBall6,noBall7);
        
        wideMenuButton = new MenuButton("WIDE");
        wideMenuButton.setFont(wickFont);
        wideMenuButton.setPrefWidth(150);

        MenuItem wide1 = new MenuItem("1");
        wide1.setStyle("-fx-font-size: 20px;");
        MenuItem wide2 = new MenuItem("2");
        wide2.setStyle("-fx-font-size: 20px;");
        MenuItem wide3 = new MenuItem("3");
        wide3.setStyle("-fx-font-size: 20px;");
        MenuItem wide4 = new MenuItem("4");
        wide4.setStyle("-fx-font-size: 20px;");
        MenuItem wide5 = new MenuItem("5");
        wide5.setStyle("-fx-font-size: 20px;");
        MenuItem wide6 = new MenuItem("6");
        wide6.setStyle("-fx-font-size: 20px;");

        // Add menu items to the MenuButton
        wideMenuButton.getItems().addAll(wide1,wide2,wide3,wide4,wide5,wide6);

        wide1.setOnAction(e -> {
            addExtraruns(1);
        });
        wide2.setOnAction(e -> {
            addExtraruns(2);
        });
        wide3.setOnAction(e -> {
            addExtraruns(3);
        });
        wide4.setOnAction(e -> {
            addExtraruns(4);
        });
        wide5.setOnAction(e -> {
            addExtraruns(5);
        });
        wide6.setOnAction(e -> {
            addExtraruns(6);
        });

        byesMenuButton = new MenuButton("BYES");
        byesMenuButton.setFont(wickFont);
        byesMenuButton.setPrefWidth(150);

        MenuItem byes1 = new MenuItem("BYES 1");
        byes1.setStyle("-fx-font-size: 20px;");
        MenuItem byes2 = new MenuItem("BYES 2");
        byes2.setStyle("-fx-font-size: 20px;");
        MenuItem byes3 = new MenuItem("BYES 3");
        byes3.setStyle("-fx-font-size: 20px;");
        MenuItem byes4 = new MenuItem("BYES 4");
        byes4.setStyle("-fx-font-size: 20px;");
        MenuItem byes5 = new MenuItem("BYES 5");
        byes5.setStyle("-fx-font-size: 20px;");
        MenuItem byes6 = new MenuItem("BYES 6");
        byes6.setStyle("-fx-font-size: 20px;");
        
       
      
        byesMenuButton.getItems().addAll(byes1,byes2,byes3,byes4,byes5,byes6);

        byes1.setOnAction(e -> {
            addbyesruns(1);
        });
        byes2.setOnAction(e -> {
            addbyesruns(2);
        });
        byes3.setOnAction(e -> {
            addbyesruns(3);
        });
        byes4.setOnAction(e -> {
            addbyesruns(4);
        });
        byes5.setOnAction(e -> {
            addbyesruns(5);
        });
        byes6.setOnAction(e -> {
            addbyesruns(6);
        });



        wicketMenuButton = new MenuButton("WICKET");
        wicketMenuButton.setFont(wickFont);
        wicketMenuButton.setPrefWidth(150);

        MenuItem lbw = new MenuItem("LBW");
        lbw.setStyle("-fx-font-size: 20px;");
        MenuItem bold = new MenuItem("BOLD");
        bold.setStyle("-fx-font-size: 20px;");
        MenuItem catchout = new MenuItem("CATCH");
        catchout.setStyle("-fx-font-size: 20px;");
        MenuItem runout = new MenuItem("RUNOUT");
        runout.setStyle("-fx-font-size: 20px;");
        MenuItem hit = new MenuItem("HIT");
        hit.setStyle("-fx-font-size: 20px;");
        
        lbw.setOnAction(e->{
            submitWicket(0,"Lbw");


        });
        bold.setOnAction(e->{
            submitWicket(0,"Bold");
        });
        catchout.setOnAction(e->{
            submitWicket(0,"Catch");
        });
      
        runout.setOnAction(e -> {
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Runout");

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window");

            Label runoutLabel = new Label("!!! Runout !!!\n\n\n");
            runoutLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 40));
            TextField runsScoredInput = new TextField();
            runsScoredInput.setFont(boldFont);
            runsScoredInput.setMaxWidth(150);
            runsScoredInput.setMinHeight(40);
            runsScoredInput.setAlignment(Pos.CENTER);
            runsScoredInput.setPromptText("Enter runs scored");
        
            Button submitButton = new Button("Submit");
            submitButton.setFont(boldFont);
            submitButton.getStyleClass().add("button");

            submitButton.setOnAction(submitEvent -> {
                    popupStage.close();
                
            });

            Label EnterRuns = new Label("Enter Runs Scored\n\n");
            EnterRuns.setFont(Font.font("Arial", FontWeight.BOLD, 30));
            popupLayout.getChildren().addAll(runoutLabel,EnterRuns, runsScoredInput,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700, 500);
            // Screen screen = Screen.getPrimary();
            // Rectangle2D bounds = screen.getVisualBounds();
            // double xOffset = bounds.getWidth() - popupLayout.getWidth() - 130; // Adjust as needed
            // double yOffset = bounds.getHeight() - popupLayout.getHeight() - 100; // Adjust as needed
            // popupStage.setX(xOffset);
            // popupStage.setY(yOffset);

            popupStage.setScene(popupScene);
            popupStage.showAndWait();
            int runs = Integer.parseInt(runsScoredInput.getText());

            submitWicket(runs,"Runout");
        });

        hit.setOnAction(e->{

            submitWicket(0,"Hit");
        });

        // Add menu items to the MenuButton
        wicketMenuButton.getItems().addAll(lbw,bold,catchout,runout,hit);

        strikeMenuButton = new MenuButton("STRIKE");

        strikeMenuButton.setFont(wickFont);
        strikeMenuButton.setPrefWidth(150);

        MenuItem p1 = new MenuItem("Player1");
        p1.setStyle("-fx-font-size: 20px;");
        MenuItem p2 = new MenuItem("Player2");
        p2.setStyle("-fx-font-size: 20px;");

        strikeMenuButton.getItems().addAll(p1,p2);

        runsHBox1 = new HBox(30);
        runsHBox1.setAlignment(Pos.TOP_CENTER);
        runsHBox1.setPadding(new Insets(10,0,0,0));
        runsHBox1.getChildren().addAll(dotButton,oneRunButton,twoRunButton,threeRunButton,fourRunButton,fiveRunButton,sixRunButton);

        runsHBox2 = new HBox(30);
        runsHBox2.setAlignment(Pos.TOP_CENTER);
        runsHBox2.setPadding(new Insets(60,0,0,0));
        runsHBox2.getChildren().addAll(noBallMenuButton,wicketMenuButton,wideMenuButton,byesMenuButton);

        strikeHBox = new HBox(30);
        strikeHBox.setPadding(new Insets(70, 0, 0, 300));
        strikeHBox.getChildren().addAll(strikeMenuButton,undoButton);

        dataEntryLayout = new VBox(10);
        runsHBox1.setPadding(new Insets(10,0,0,0));

        // removed striker menu box for further changes 
        dataEntryLayout.getChildren().addAll(dataEntryLabel,runsHBox1,runsHBox2);



    // ****************************************************************************************************ScoreBoard Layout 

        scoreboardLayout = new VBox(10);
        runsTextArea = new TextArea();
        runsTextArea.setEditable(false);

        runRateTextArea = new TextArea();
        runRateTextArea.setEditable(false);

        player1RunsTextArea = new TextArea();
        player1RunsTextArea.setEditable(false);

        player2RunsTextArea = new TextArea();
        player2RunsTextArea.setEditable(false);
        
        wicketsTextArea = new TextArea();
        wicketsTextArea.setEditable(false);
        
        bowler1InfoTextArea = new TextArea();
        bowler2InfoTextArea = new TextArea();
        
        lastSixBallsTextArea = new TextArea();

        VBox TeamNameVBox = ScoreCard.TeamDisp(Team1,Team2);
        RunRate = ScoreCard.runrateDisp(0.0);
        RunRate.setStyle("-fx-font-size:20px; -fx-font-weight:bold; -fx-text-fill: white;");
        
        VBox nameRunVBox = new VBox(20);
        nameRunVBox.getChildren().addAll(TeamNameVBox,RunRate);
        scoreLabel = ScoreCard.DispScore(0,0,0);
        //scoreLabel.setFont(nameFont);
        scoreLabel.setStyle("-fx-font-size:50px; -fx-font-weight:bold; -fx-text-fill: white;");

        nameScoreVBox = new HBox();
        nameScoreVBox.setPadding(new Insets(50,0,0,50));
        nameScoreVBox.getChildren().addAll(nameRunVBox,scoreLabel);
        
        VBox battersVBox = new VBox(20);


        batters = new Label("Batters");
        batters.setFont(boldFont);
        batters.setStyle("-fx-font-size:24px; -fx-font-weight:bold; -fx-text-fill: white;");
        name1 = new Label(" * "+ Bat1);
        name2 = new Label(Bat2);
        name1.setFont(name);
        name2.setFont(name);
        name1.setStyle("-fx-font-size:20px; -fx-font-weight:bold; -fx-text-fill: white;");
        name2.setStyle("-fx-font-size:20px; -fx-font-weight:bold; -fx-text-fill: white;");

        //nameVBox = new VBox(10);
        battersVBox.getChildren().addAll(batters,name1,name2);



        batBoldLabel = new Label();
        batBoldLabel.setText("  R                  B                 4s                 6s                 SR");
        batBoldLabel.setFont(boldFont);
        batBoldLabel.setStyle("-fx-font-size:24px; -fx-font-weight:bold; -fx-text-fill: white;");

        bat1Label = new Label();
        bat2Label = new Label();
        bat1Label.setFont(name);
        bat2Label.setFont(name); 
        bat1Label.setText("  "+ Batsman1runs +"                       "+ Batsman1balls +"                       "+ Batsman1fours +"                         "+ Batsman1six +"                        "+ Batsman1StrikeR);
        bat2Label.setText("  "+ Batsman2runs +"                       "+ Batsman2balls +"                       "+ Batsman2fours +"                         "+ Batsman2six +"                        "+ Batsman2StrikeR);
        bat1Label.setStyle("-fx-text-fill: white;");
        bat2Label.setStyle("-fx-text-fill: white;");

        dataVBox = new VBox(20);
        dataVBox.getChildren().addAll(batBoldLabel,bat1Label,bat2Label);

        battersBox = new HBox(30);
        battersBox.setPadding(new Insets(50,0,0,50));
        battersBox.getChildren().addAll(battersVBox,dataVBox);



        ballBoldLabel = new Label();
        ballBoldLabel.setText("O                  M                  R                  W                  ECO");
        ballBoldLabel.setFont(boldFont);
        ballBoldLabel.setStyle("-fx-font-size:24px; -fx-font-weight:bold; -fx-text-fill: white;");

        ball1Label = new Label();
        ball1Label.setFont(name);
        ball1Label.setStyle("-fx-text-fill: white;");
        ball1Label.setText(Baller1overs+"                     "+0+"                     "+Baller1runsconceeded+"                         "+Baller1wickets+"                        "+Baller1economy);
        ballDataVBox = new VBox(10);
        ballDataVBox.getChildren().addAll(ballBoldLabel,ball1Label);


        BowlerLabel = new Label("Bowlers");
        BowlerLabel.setFont(boldFont);
        BowlerLabel.setStyle("-fx-font-size:24px; -fx-font-weight:bold; -fx-text-fill: white;");

        ballname = new Label(BowlerName1);
        ballname.setFont(name);
        ballname.setStyle("-fx-font-size:20px; -fx-font-weight:bold; -fx-text-fill: white;");
        ballNameVBox = new VBox(10);
        ballNameVBox.getChildren().addAll(BowlerLabel,ballname);
    
        //VBox ballDataVBox1 = ScoreCard.updateBalls(0.0,0,0,0,0.0);
        HBox ballersBox1 = new HBox(70);
        ballersBox1.setPadding(new Insets(50,0,0,50));
        ballersBox1.getChildren().addAll(ballNameVBox,ballDataVBox);

        Separator horizontalLine = new Separator();
        horizontalLine.setStyle("-fx-border-width: 2px;-fx-border-color: black; -fx-background-color: black");

        // HBox br = new HBox();
        // Label Br = new Label("\n\n\n");
        // br.getChildren().addAll(Br);

        lastVBox = new VBox(5);
        lastVBox.getChildren().addAll(ballersBox1);
        
        Image backgroundImage = new Image("Images/v1.jpg");
        BackgroundImage background = new BackgroundImage(backgroundImage,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(950, 1050, false, false, false, false));

        Image backgroundImage1 = new Image("Images/r14.jpg");
        BackgroundImage background1 = new BackgroundImage(backgroundImage1,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(950, 1050, false, false, false, false));


        scoreboardLayout.getChildren().addAll(nameScoreVBox,battersBox,lastVBox);
        scoreboardLayout.setBackground(new Background(background));
        //scoreboardLayout.getStylesheets().add("styles.css");
        dataEntryLayout.setBackground(new Background(background1));

        // **********************************************************************************   Start   ->  Create a SplitPane to arrange the windows side by side

        splitPane = new SplitPane(scoreboardLayout,dataEntryLayout);
        splitPane.setDividerPositions(0.5);     // Adjust the divider position as needed 

        // Create the scene
        Scene scene = new Scene(splitPane, 1920, 990);
        window.setScene(scene);
        window.setMaximized(true);
        window.show();
       
    }

    public static Button createRunsButton(String text, int runs) {

        Button button = new Button(text);
        button.setOnAction(e -> submitRuns(runs));
         return button;
    }
    public static void addExtraruns(int runs){
        Runs = Runs + runs;
        scoreLabel.setText("          "+ Runs + " / "+ wickets + "( " + over + "." + balls + " )");
        String Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
        RunRate.setText(Runrate);
        
    }
    public static void addbyesruns(int runs){
        balls++;
        Runs = Runs + runs;
        scoreLabel.setText("          "+ Runs +" / "+ wickets + "( " + over + "." + balls + " )");
        String Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
        RunRate.setText(Runrate);
        calculateBowler1Economy();
        
        if(balls == 6){
            
            over++;
            balls = 0;
            Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
            RunRate.setText(Runrate);

            if(over == TotaloversMatch){
                scoreLabel.setText("          "+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");
                End1stEnning();
            }
            scoreLabel.setText("          "+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");

            if(over < TotaloversMatch){
                UpdateBaller();
            }
            
        }
    }

    public static Button createWicketButton(int runs,String WicketType) {
        Button button = new Button("Wicket");
        button.setOnAction(e -> submitWicket(0,WicketType));
        return button;
    }

    public static void submitRuns(int runs) {
        Runs = Runs + runs;
        balls++;
        String Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
        RunRate.setText(Runrate);
        Baller1runsconceeded = Baller1runsconceeded + runs;


        //  Start ->  ******************************************************************************************        Add * in front of striker batsman
        if(runs % 2 == 1 &&  balls != 6){
            if(StrikeTemp == 0){
                name2.setText(" * " + Batsman2);
                name1.setText("  " + Batsman1);
            }
            else{
                
                name1.setText( "* " + Batsman1);
                name2.setText(" " + Batsman2);
            }
                
        }  
        else if(runs%2==0 && balls == 6){
            if(StrikeTemp == 0){
                name2.setText("* " + Batsman2);
                name1.setText("  " + Batsman1);
            }
            else{
                
                name1.setText( "* " + Batsman1);
                name2.setText(" " + Batsman2);
            }
        }

        //  End  -> ********************************************************************************************   END
        
        
        // *******************************************************************************************  Start    ->   Change Batsman Data Which is on Strikec After Ball  And update Bowler Data
        

        calculateBowler1Economy();         //   Bowler Economy
        

        if(StrikeTemp == 0){
            

            if(runs == 4){
                Batsman1fours++;
            }
            if(runs == 6){
                Batsman1six++;
            }
            Batsman1runs = Batsman1runs + runs;
            Batsman1balls++;
            double StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
            String str = String.format("%.2f", StrikeRate);
            int Matchid = CreateMatch.matchid();
            BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str,"notout");
            
            // int Match_id = CreateMatch.matchid();
            // BattingDataUpdater.updateBattingData(Match_id,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six);
            bat1Label.setText("  "+ Batsman1runs +"                       "+ Batsman1balls +"                       "+ Batsman1fours +"                         "+ Batsman1six +"                        "+ str);

            if(runs%2 == 1 &&  balls != 6){
                StrikeTemp = 1;
            }
            if(runs % 2 == 0 && balls == 6){
                StrikeTemp = 1;
            }

        }
        else if(StrikeTemp == 1){
            if(runs == 4){
                Batsman2fours++;
            }
            if(runs == 6){
                Batsman2six++;
            }
            Batsman2runs = Batsman2runs + runs;
            Batsman2balls++;

            double StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
            String str = String.format("%.2f", StrikeRate);

            int Matchid = CreateMatch.matchid();
            BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str,"notout");

            bat2Label.setText("  "+ Batsman2runs +"                       "+ Batsman2balls +"                       "+ Batsman2fours +"                         "+ Batsman2six +"                        "+ str);

            if(runs%2 == 1 &&  balls != 6){
                StrikeTemp = 0;
            }
            
            if(runs % 2 == 0 && balls == 6){
                StrikeTemp = 0;
            }

        }

        //************************************************************************************************ END 

        // *********************************************************************************************** Start     ->    Change Bowler After Completed an Over
        if(balls == 6){
            
            over++;
            balls = 0;
            Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
            RunRate.setText(Runrate);
            System.out.println("hii");
            System.out.println(over);
            System.out.println(TotaloversMatch);
            if(over == TotaloversMatch){
                scoreLabel.setText("          "+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");
                End1stEnning();
            }
            scoreLabel.setText("          "+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");

            if(over < TotaloversMatch){
                UpdateBaller();
            }
            
        }
        System.out.println(Runs);

        // ************************************************************************************************ END 

        
        // **** ->  Update ScoreBoard 
        
        scoreLabel.setText("          "+ Runs +" / "+ wickets + "( " + over + "." + balls + " )");

    }

    // ********************************************************************************************   Start ->  Update Wickets Of Batting Team And the Bowler Wickets  And Add New Batsman   

    private static void submitWicket(int runs,String Wickettype) {
        wicketsTextArea.setText("Wickets: W");
        Runs = Runs + runs;
        wickets++;
        

        if(Wickettype.equals("Runout")){
            if(StrikeTemp == 0){
                Batsman1runs = Batsman1runs + runs;
                Batsman1balls++;
                double StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
                String str = String.format("%.2f", StrikeRate);
                
                int Matchid = CreateMatch.matchid();
                BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str,"Run Out");
                // int Match_id = CreateMatch.matchid();
                // BattingDataUpdater.updateBattingData(Match_id,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six);
                bat1Label.setText("  "+ Batsman1runs +"                       "+ Batsman1balls +"                       "+ Batsman1fours +"                         "+ Batsman1six +"                        "+ str);
                
            }
            else{

                Batsman2runs = Batsman2runs + runs;
                Batsman2balls++;
                double StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
                String str = String.format("%.2f", StrikeRate);
                int Matchid = CreateMatch.matchid();
                BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str,"Run Out");
                
                bat2Label.setText("  "+ Batsman2runs +"                       "+ Batsman2balls +"                       "+ Batsman2fours +"                         "+ Batsman2six +"                        "+ str);
            }
        }
        else{
            Baller1wickets++;
        }
       
        
        balls++;

        String Runrate = ScoreCard.TeamRunRate(Runs,over,balls);
        RunRate.setText(Runrate);

        // *   --> update Bowler Economy
        Baller1runsconceeded = Baller1runsconceeded + runs;
        calculateBowler1Economy();

        
        
        
        
        
        

        scoreLabel.setText("          "+ Runs +" / "+ wickets + "( " + over + "." + balls + " )");
       

        Stage SlectPlayerPopup = new Stage();
            SlectPlayerPopup.initModality(Modality.APPLICATION_MODAL);
            SlectPlayerPopup.setTitle("New Batsman");
            

            VBox popupLayout = new VBox(10);
            popupLayout.getStyleClass().add("popup-window"); // Apply the CSS class

            Label AddBatsman = new Label(" Select New Batsman\n\n\n");
            AddBatsman.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));

            ComboBox<String> Team1remainingPlayerComboBox = new ComboBox<>();
            Team1remainingPlayerComboBox.getStyleClass().add("combo-box"); // Apply the ComboBox style
            ComboBox<String> OutPlayer = new ComboBox<>();
            Label SelectOutPlayer = new Label(" Choose The Batsman Out\n\n");
            SelectOutPlayer.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
            
            if(Wickettype.equals("Runout")){
                OutPlayer.getItems().add(Batsman1);
                OutPlayer.getItems().add(Batsman2);
            }
            
            else if(StrikeTemp == 0){
                outBat = Batsman1;
            }
            else if(StrikeTemp == 1){
                outBat = Batsman2;
            }


            for(int i = 0;i<Team1arr.length;i++){
                Team1remainingPlayerComboBox.getItems().add(Team1arr[i]);
            }

            Button submitButton = new Button("Submit");
            submitButton.setOnAction(submitEvent -> {
                Team1remainingPlayerComboBox.getValue();
                if(Wickettype.equals("Runout")){    
                    OutPlayer.getValue();
                    if((OutPlayer.getValue()).equals(Batsman1)){
                        Batsman1balls++;
                        double StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
                        String str = String.format("%.2f", StrikeRate);
                        int Matchid = CreateMatch.matchid();
                        BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str,Wickettype + " / "+BowlerName1);

                        Batsman1 = Team1remainingPlayerComboBox.getValue();
                        //****************************************************************************************** 

                        //   Add updates in database

                        // *****************************************************************************************

                        Batsman1StrikeR = 0.0;
                        Batsman1balls = 0;
                        Batsman1fours = 0;
                        Batsman1runs = 0;
                        Batsman1six = 0;

                        StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
                        str = String.format("%.2f", StrikeRate);
                        name1.setText(" " + Batsman1); 
                        bat1Label.setText("  "+ Batsman1runs +"                        "+ Batsman1balls +"                        "+ Batsman1fours +"                         "+ Batsman1six +"                         "+ str);


                        
                    }
                    else if((OutPlayer.getValue()).equals(Batsman2)){

                        Batsman2balls++;
                        double StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
                        String str = String.format("%.2f", StrikeRate);
                        int Matchid = CreateMatch.matchid();
                        BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str,Wickettype + " / "+BowlerName1);

                        Batsman2 = Team1remainingPlayerComboBox.getValue();

                        Batsman2StrikeR = 0.0;
                        Batsman2balls = 0;
                        Batsman2fours = 0;
                        Batsman2runs = 0;
                        Batsman2six = 0;
                        
                        StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
                        str = String.format("%.2f", StrikeRate);
                        name2.setText(" " + Batsman2);
                        bat2Label.setText("  "+ Batsman2runs +"                       "+ Batsman2balls +"                       "+ Batsman2fours +"                         "+ Batsman2six +"                        "+ str);
                    }
                    
                    
                }

                else{
                    if(outBat.equals(Batsman1)){
                            Batsman1 = Team1remainingPlayerComboBox.getValue();
                            //****************************************************************************************** 

                            //   Add updates in database

                            // *****************************************************************************************

                            Batsman1StrikeR = 0.0;
                            Batsman1balls = 0;
                            Batsman1fours = 0;
                            Batsman1runs = 0;
                            Batsman1six = 0;

                            double StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
                            String str = String.format("%.2f", StrikeRate);
                            
                            if(balls != 6){
                                name1.setText("* " + Batsman1);
                            }
                            else{
                                name1.setText(" " + Batsman1);
                                name2.setText("* " + Batsman2);
                            }
                            
                            bat1Label.setText("  "+ Batsman1runs +"                      "+ Batsman1balls +"                       "+ Batsman1fours +"                         "+ Batsman1six +"                        "+ str);
                    }
                    else if(outBat.equals(Batsman2)){
                        Batsman2 = Team1remainingPlayerComboBox.getValue();
                        
                        Batsman2StrikeR = 0.0;
                        Batsman2balls = 0;
                        Batsman2fours = 0;
                        Batsman2runs = 0;
                        Batsman2six = 0;
                        
                        double StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
                        String str = String.format("%.2f", StrikeRate);
                        if(balls != 6){
                            name2.setText("* " + Batsman2);
                        }
                        else{
                            name1.setText("* " + Batsman1);
                            name2.setText(" " + Batsman2);
                        }
                        bat2Label.setText("  "+ Batsman2runs +"                       "+ Batsman2balls +"                       "+ Batsman2fours +"                         "+ Batsman2six +"                        "+ str);
                    }
                    
                }
                
                String remainingPlayer[] = new String[Team1arr.length - 1 ];
                int k = 0;
                for(int i = 0;i<Team1arr.length;i++){
                    if(Team1arr[i].equals(Team1remainingPlayerComboBox.getValue()) == false){
                        remainingPlayer[k] = Team1arr[i];
                        k++;
                    }
                }
                Team1arr = remainingPlayer;
                SlectPlayerPopup.close();

                
            });

        if(Wickettype.equals("Runout")){
            popupLayout.getChildren().addAll(SelectOutPlayer,OutPlayer,AddBatsman,Team1remainingPlayerComboBox,submitButton);
        }
        else{
            popupLayout.getChildren().addAll(AddBatsman,Team1remainingPlayerComboBox,submitButton);
        }
        popupLayout.setAlignment(Pos.CENTER);

        popupLayout.getStylesheets().add("styles.css");
        Scene popupScene = new Scene(popupLayout, 700, 500);
        //Screen screen = Screen.getPrimary();
        // Rectangle2D bounds = screen.getVisualBounds();
        // double xOffset = bounds.getWidth() - popupLayout.getWidth() - 130; // Adjust as needed
        // double yOffset = bounds.getHeight() - popupLayout.getHeight() - 100; // Adjust as needed
        // SlectPlayerPopup.setX(xOffset);
        // SlectPlayerPopup.setY(yOffset);

        SlectPlayerPopup.setScene(popupScene);
        SlectPlayerPopup.showAndWait();

        if(wickets == 10){
                System.out.println("First Inning Over Start Second Enning ");
                End1stEnning();
        }

        if(balls == 6){
            over++;
            balls = 0;
            if((over == TotaloversMatch) || (wickets == 10)){
                System.out.println("First Inning Over Start Second Enning ");
                scoreLabel.setText("          "+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");
                End1stEnning();
            }
            scoreLabel.setText("          "+ Runs +" / "+ wickets + "( " + over + "." + 0 + " )");
            if(over < TotaloversMatch && wickets < 10){
                UpdateBaller();
            }
            
        }


    }

    // End  ->  ***********************************************************************************************************************************************   End   

    
    public static VBox updateRuns(int runs, int ball, int four, int six,double SR,int i){
        
        batBoldLabel = new Label();
        batBoldLabel.setText("  R                  B                  4s                  6s                  SR");
        batBoldLabel.setFont(boldFont);

        bat1Label = new Label();
        bat2Label = new Label();
        if(i == 0){
            bat1Label.setText("  "+runs+"                        "+ball+"                       "+four+"                         "+six+"                        "+SR);
        }
        else{
            bat2Label.setText("  "+runs+"                       "+ball+"                       "+four+"                         "+six+"                        "+SR);
        }

        dataVBox = new VBox(10);
        dataVBox.getChildren().addAll(batBoldLabel,bat1Label,bat2Label);
        return dataVBox;
    }



    // Start  ->   ***********************************************************************************************     ->   Update Baller When Over Finished  
    public static void UpdateBaller(){
        
        if(over < TotaloversMatch){
            balls = 0;
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Update Bowler");

            VBox popupLayout = new VBox(20);
            popupLayout.getStyleClass().add("popup-window");
            Label OverCompleted = new Label("Over Completed \n Choose Bowler!\n\n\n");
            OverCompleted.setFont(Font.font("Times New Roman", FontWeight.BOLD, 35));
            String[] BowlingTeam = Team2arr;

            ComboBox <String> BowlerCombobox = new ComboBox<>();

            for(int i = 0;i<BowlingTeam.length;i++){
                if(BowlingTeam[i].equals(BowlerName1) == false){
                    BowlerCombobox.getItems().add(BowlingTeam[i]);
                }
            }



            Button submitButton = new Button("Submit");
            submitButton.setOnAction(submitEvent -> {

                ballname.setText(BowlerCombobox.getValue());
                BowlerName1   = BowlerCombobox.getValue();
                Baller1economy = 0.0;
                Baller1overs = 0;
                Baller1runsconceeded = 0;
                Baller1wickets = 0;
                
                ball1Label.setText(Baller1overs + "." + balls +"                       "+0+"                       "+Baller1runsconceeded+"                           "+Baller1wickets+"                          "+Baller1economy);

                popupStage.close();
                
            });

            popupLayout.getChildren().addAll(OverCompleted,BowlerCombobox,submitButton);
            popupLayout.setAlignment(Pos.CENTER);
            popupLayout.getStylesheets().add("styles.css");
            Scene popupScene = new Scene(popupLayout, 700, 500);
            //Screen screen = Screen.getPrimary();
            // Rectangle2D bounds = screen.getVisualBounds();
            // double xOffset = bounds.getWidth() - popupLayout.getWidth() - 130; // Adjust as needed
            // double yOffset = bounds.getHeight() - popupLayout.getHeight() - 100; // Adjust as needed
            // popupStage.setX(xOffset);
            // popupStage.setY(yOffset);

            popupStage.setScene(popupScene);
            popupStage.showAndWait();
        }


    }

    // End ->  **********************************************************************************************************    END   

    //  Start   ->  *******************************************************************************************************************   Baller Economy 
    public static void calculateBowler1Economy() {
        double economy1 = 0.0;
        
        
        double oversBowled = Baller1overs + (balls / 6.0); // Calculate total overs bowled by Bowler
        
        if (oversBowled > 0) {
            double runsConceded = Baller1runsconceeded;
            economy1 =  runsConceded / oversBowled;
        } else {
            economy1 = 0.0;
        }
        ball1Label.setText(Baller1overs + "." + balls +"                       "+0+"                       "+Baller1runsconceeded+"                           "+Baller1wickets+"                          "+economy1);

    }
    
    public static void End1stEnning(){
        System.out.println("Enning 1 End Now Start Enning 2");

        // ***************************************************************************************   Get Information About 2nd innings Batsman1 Batsman2 and Bowler
        Stage SlectPlayerPopup = new Stage();
        SlectPlayerPopup.initModality(Modality.APPLICATION_MODAL);
        SlectPlayerPopup.setTitle("Start 2nd Inning");
        Label TargetLabel = new Label(" Target For " + team2 + " is " + (Runs+1) + " To Win \n\n");
        TargetLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 35));
        VBox popupLayout = new VBox(20);
        popupLayout.getStyleClass().add("popup-window");

        Label Message = new Label("!!! Starts Second Inning !!! \n\n");
        Message.setFont(Font.font("Calibri", FontWeight.BOLD, 50));

        Label Batsman1 = new Label("Select Striker Batsman");
        Batsman1.setFont(Font.font("Calibri", FontWeight.BOLD, 25));
        Label Batsman2 = new Label("Select Non-Striker Batsman");
        Batsman2.setFont(Font.font("Calibri", FontWeight.BOLD, 25));
        
        ComboBox<String> Batsman1ComboBox = new ComboBox<>();
        ComboBox<String> Batsman2ComboBox = new ComboBox<>();
        Batsman1ComboBox.setPromptText(" Select Striker Batsman ");
        Batsman2ComboBox.setPromptText(" Select Non-Striker Batsman ");
        //Batsman1ComboBox.setPromptText(" Select Striker Batsman ");
        Label Bowler = new Label(" Select Bowler ");
        Bowler.setFont(Font.font("Calibri", FontWeight.BOLD, 25));
        ComboBox<String> FirstBowler = new ComboBox<>();
        FirstBowler.setPromptText(" Select Bowler ");

        Batsman1ComboBox.getItems().addAll(for2ndEnningBattingTeam[0],for2ndEnningBattingTeam[1],for2ndEnningBattingTeam[2],for2ndEnningBattingTeam[3],for2ndEnningBattingTeam[4],for2ndEnningBattingTeam[5],for2ndEnningBattingTeam[6],for2ndEnningBattingTeam[7],for2ndEnningBattingTeam[8],for2ndEnningBattingTeam[9],for2ndEnningBattingTeam[10],for2ndEnningBattingTeam[11],for2ndEnningBattingTeam[12]);
        Batsman2ComboBox.getItems().addAll(for2ndEnningBattingTeam[0],for2ndEnningBattingTeam[1],for2ndEnningBattingTeam[2],for2ndEnningBattingTeam[3],for2ndEnningBattingTeam[4],for2ndEnningBattingTeam[5],for2ndEnningBattingTeam[6],for2ndEnningBattingTeam[7],for2ndEnningBattingTeam[8],for2ndEnningBattingTeam[9],for2ndEnningBattingTeam[10],for2ndEnningBattingTeam[11],for2ndEnningBattingTeam[12]);
        FirstBowler.getItems().addAll(for2ndEnningBowlingTeam[0],for2ndEnningBowlingTeam[1],for2ndEnningBowlingTeam[2],for2ndEnningBowlingTeam[3],for2ndEnningBowlingTeam[4],for2ndEnningBowlingTeam[5],for2ndEnningBowlingTeam[6],for2ndEnningBowlingTeam[7],for2ndEnningBowlingTeam[8],for2ndEnningBowlingTeam[9],for2ndEnningBowlingTeam[10],for2ndEnningBowlingTeam[11],for2ndEnningBowlingTeam[12]);
            


        Button submitButton = new Button("Start 2nd Inning ");
        submitButton.setOnAction(submitEvent -> {
                inning2.ScoreBoard2(window, team1, team2, tosswinner, Choose, Batsman1ComboBox.getValue(), Batsman2ComboBox.getValue(), FirstBowler.getValue(), for2ndEnningBattingTeam, for2ndEnningBowlingTeam, TotaloversMatch,Runs,wickets,over,balls);
                SlectPlayerPopup.close();
        });

        HBox striker = new HBox(30);
        striker.setAlignment(Pos.CENTER);
        striker.getChildren().addAll(Batsman1ComboBox,Batsman2ComboBox);

        HBox bowler = new HBox();
        bowler.setAlignment(Pos.CENTER);
        bowler.getChildren().add(FirstBowler);

        HBox submitBox = new HBox();
        submitBox.setAlignment(Pos.CENTER);
        submitBox.getChildren().add(submitButton);

        popupLayout.getChildren().addAll(Message,TargetLabel,striker,bowler,submitBox);
        popupLayout.setAlignment(Pos.CENTER);
        popupLayout.getStylesheets().add("styles.css");
        Scene popupScene = new Scene(popupLayout, 700, 500);
        // Screen screen = Screen.getPrimary();
        // Rectangle2D bounds = screen.getVisualBounds();
        // double xOffset = bounds.getWidth() - popupLayout.getWidth() - 130; // Adjust as needed
        // double yOffset = bounds.getHeight() - popupLayout.getHeight() - 100; // Adjust as needed
        // SlectPlayerPopup.setX(xOffset);
        // SlectPlayerPopup.setY(yOffset);

        SlectPlayerPopup.setScene(popupScene);
        SlectPlayerPopup.showAndWait();

    }
    public static void noballbatsmandataupdate(int runs){
        
        if(runs % 2 == 1 &&  balls != 6){
            if(StrikeTemp == 0){
                name2.setText(" * " + Batsman2);
                name1.setText("  " + Batsman1);
            }
            else{
                
                name1.setText( "* " + Batsman1);
                name2.setText(" " + Batsman2);
            }
                
        }  
        else if(runs%2==0 && balls == 6){
            if(StrikeTemp == 0){
                name2.setText("* " + Batsman2);
                name1.setText("  " + Batsman1);
            }
            else{
                
                name1.setText( "* " + Batsman1);
                name2.setText(" " + Batsman2);
            }
        }

        if(StrikeTemp == 0){
            

            if(runs == 4){
                Batsman1fours++;
            }
            if(runs == 6){
                Batsman1six++;
            }
            Batsman1runs = Batsman1runs + runs;
            Batsman1balls++;
            double StrikeRate = (Batsman1runs * 100.0) / Batsman1balls;
            String str = String.format("%.2f", StrikeRate);
            int Matchid = CreateMatch.matchid();
            BattingDataUpdater.updateBattingData(Matchid,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six,str,"notout");
            
            // int Match_id = CreateMatch.matchid();
            // BattingDataUpdater.updateBattingData(Match_id,Batsman1,Batsman1runs,Batsman1balls,Batsman1fours,Batsman1six);
            bat1Label.setText("  "+ Batsman1runs +"                       "+ Batsman1balls +"                       "+ Batsman1fours +"                         "+ Batsman1six +"                        "+ str);

            if(runs%2 == 1 &&  balls != 6){
                StrikeTemp = 1;
            }
            if(runs % 2 == 0 && balls == 6){
                StrikeTemp = 1;
            }

        }
        else if(StrikeTemp == 1){
            if(runs == 4){
                Batsman2fours++;
            }
            if(runs == 6){
                Batsman2six++;
            }
            Batsman2runs = Batsman2runs + runs;
            Batsman2balls++;

            double StrikeRate = (Batsman2runs * 100.0) / Batsman2balls;
            String str = String.format("%.2f", StrikeRate);

            int Matchid = CreateMatch.matchid();
            BattingDataUpdater.updateBattingData(Matchid,Batsman2,Batsman2runs,Batsman2balls,Batsman2fours,Batsman2six,str,"notout");

            bat2Label.setText("  "+ Batsman2runs +"                       "+ Batsman2balls +"                       "+ Batsman2fours +"                         "+ Batsman2six +"                        "+ str);

            if(runs%2 == 1 &&  balls != 6){
                StrikeTemp = 0;
            }
            
            if(runs % 2 == 0 && balls == 6){
                StrikeTemp = 0;
            }

        }
    }
}

