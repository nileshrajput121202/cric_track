package Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BattingDataUpdater {


    public static void updateBattingData(int matchId, String playerName, int runs, int ballsFaced, int fours, int sixes, String strikeRate, String dismisalType) {
        String dbUrl = "jdbc:mysql://localhost:3306/crictrack";
        String dbUser = "root";
        String dbPassword = "Pass@123";
    
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword)) {
            String updateQuery = "UPDATE players " +
                "SET RunScored = ?, BallsFaced = ?, Sixes = ?, Fours = ?, StrikeRate = ?, DismisalType = ? " +
                "WHERE Match_id = ? AND PlayerName = ?";
    
            try (PreparedStatement preparedStatement = connection.prepareStatement(updateQuery)) {
                preparedStatement.setInt(1, runs);
                preparedStatement.setInt(2, ballsFaced);
                preparedStatement.setInt(3, sixes);
                preparedStatement.setInt(4, fours);
                preparedStatement.setString(5, strikeRate);
                preparedStatement.setString(6, dismisalType);
                preparedStatement.setInt(7, matchId);
                preparedStatement.setString(8, playerName);
    
                int rowsUpdated = preparedStatement.executeUpdate();
                if (rowsUpdated > 0) {
                    System.out.println("Batting data updated successfully!");
                } else {
                    System.out.println("No rows updated. Player or match not found.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}
