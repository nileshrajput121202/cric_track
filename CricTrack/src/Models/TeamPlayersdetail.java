package Models;
// package Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class TeamPlayersdetail {
    static String PlayerID;
    static String dbUrl = "jdbc:mysql://localhost:3306/crictrack";
    static String dbUser = "root";
    static String dbPassword = "Pass@123";
    static String[] PlayersTeam1 = new String[13];
    static String[] PlayersTeam2 = new String[13];

    public static String[] Team1Details(int Temp,int Matchid){

        try (Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword)) {
        String selectQuery = "SELECT * FROM players";

        try (PreparedStatement preparedStatement = connection.prepareStatement(selectQuery)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            int i = 0;
            
            while (resultSet.next()) {
                System.out.println("match id = " + resultSet.getInt("Match_id"));
                // int key = resultSet.getInt("student_id");
                // System.out.println(key);
                int key = resultSet.getInt("PlayerId");
                System.out.println(key);
                //String PlayerId = resultSet.getString("PlayerId");
                //String Match_id = resultSet.getString("Match_id");
                String PlayerName = resultSet.getString("PlayerName");
                //double TeamName = resultSet.getDouble("TeamName");
                if(i < 13 && resultSet.getInt("Match_id") == Matchid){
                    PlayersTeam1[i] = PlayerName;
                    i++;
                }
                else if(i >= 13 &&  resultSet.getInt("Match_id") == Matchid){
                    PlayersTeam2[i-13] = PlayerName;
                    i++;
                }
                
                
            }
        }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        for(int i = 0;i<13;i++){
            System.out.println(PlayersTeam1[i]);
        }
        for(int i = 0;i<13;i++){
            System.out.println(PlayersTeam2[i]);
        }
        if(Temp == 0){
            return PlayersTeam1;
        }
        else{
            return PlayersTeam2;
        }
    }
}