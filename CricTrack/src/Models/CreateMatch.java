package Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
//import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.animation.TranslateTransition;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;


public class CreateMatch {
    static Stage window;
    static Scene primaryScene;
    static String Team1 ;
    static String Team2;
    static String TossWinnerTeam;
    static String Location;
    static String MatchTime;
    static VBox inneBox2 = new VBox();
    static Object currentDetailsView;
    static Button lastClickedButton;
    static BackgroundImage backgroundImage;
    static int Matchid = -1;
    static int no = 0;
    static String Str = " Team1 ";
    static String Str2 = " Team2 ";
    static String TossWin = "";
    static int playerid = 0;
    static Label Team1Players;
    static Label Team2Players;
    static String[] Arr2;
    static String[] Arr3;
    static int overs = 0;

    static String[] BattingTeam;
    static String[] BowlingTeam;
    static String BattingTeamName;
    static String BowlingTeamName;

    public static int matchid(){
        return Matchid;
    }
    public static String BattingTeamname(){
            return BattingTeamName;
    }
    public static String BowlingTeamname(){
        return BowlingTeamName;
    }

    public static void topToBottomTranslation(TextField player) {
        TranslateTransition topToBottomTransition = new TranslateTransition(Duration.seconds(2), player);
        topToBottomTransition.setFromY(-200); // Start from the top of the screen
        topToBottomTransition.setToY(0);      // End at the original position
        topToBottomTransition.setCycleCount(1);
        topToBottomTransition.play();
    }
    public static void bottomToLeftTranslation(TextField player) {
        TranslateTransition bottomToTopTransition = new TranslateTransition(Duration.seconds(2), player);
        bottomToTopTransition.setFromY(400); // Start from the bottom of the screen
        bottomToTopTransition.setToY(0);      // End at the original position
        bottomToTopTransition.setCycleCount(1);
        bottomToTopTransition.play();
    }
    // public static int matchid(){
    //     return Matchid;
    // }

    public static void rightToLeftTranslation(ComboBox<String> Player){
         // Start the left-to-right transition for the player1
         TranslateTransition rlT = new TranslateTransition(Duration.seconds(2), Player);
         rlT.setFromX(600); // Start from the left of the screen
         rlT.setToX(0);      // End at the original position
         rlT.setCycleCount(1);
         rlT.play();
    }
    public static void setTextCenter(TextField textField){
        textField.setMaxWidth(320);
        textField.setMinHeight(40);
        textField.getStyleClass().add("text-field-with-border");
        textField.setStyle("-fx-font-size:17px; -fx-font-weight: bold;");
        textField.setAlignment(Pos.CENTER);
    }
    public static void showSuccessPopup(String message) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setX(40);
        alert.setY(700);
        
        alert.setTitle("Success");
        alert.setHeaderText(null);
        alert.setContentText(message);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add("styles.css");
        dialogPane.getStyleClass().add("styles.css");


        alert.showAndWait();
    }
    public static void showSuccessPopup1(String message) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setX(520);
        alert.setY(700);
        alert.setTitle("Success");
        alert.setHeaderText(null);
        alert.setContentText(message);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add("styles.css");
        dialogPane.getStyleClass().add("styles.css");

        alert.showAndWait();
    }
    public static void showSuccessPopup2(String message) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setX(1020);
        alert.setY(700);
        alert.setTitle("Success");
        alert.setHeaderText(null);
        alert.setContentText(message);
        
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add("styles.css");
        dialogPane.getStyleClass().add("styles.css");

        alert.showAndWait();
    }
    public static void Display(Stage primaryStage,Scene scene) throws SQLException{

        window = primaryStage;
        primaryScene = scene;
    
        VBox MatchDetails = new VBox(15);
        MatchDetails.setId("match-details-vbox");
        MatchDetails.setPadding(new Insets(10,0,0,0));

        Label Mactch_Details = new Label("Match Details");

        Mactch_Details.setStyle("-fx-font-size:37px; -fx-font-weight: bold;");
        
        Label Match1Label = new Label("Team1");
        Match1Label.setPadding(new Insets(0,270,0,0));

        Match1Label.setTextFill(Color.WHITE);

        //Match1Label.setStyle("-fx-");

        Match1Label.setStyle("-fx-font-size:19px; -fx-font-weight: bold;");
  
        TextField Team1NameTextField = new TextField("Vikings");
        //CreateMatch.topToBottomTranslation(Team1NameTextField);
        setTextCenter(Team1NameTextField);

        Label Match2Label = new Label("Team 2");
        Match2Label.setPadding(new Insets(0,260,0,0));
        Match2Label.setTextFill(Color.WHITE);

  
        Match2Label.setStyle("-fx-font-size:20px; -fx-font-weight: bold;");
        TextField Team2NameTextField = new TextField("Legecy");
        //CreateMatch.topToBottomTranslation(Team2NameTextField);
        setTextCenter(Team2NameTextField);

        Label MatchDate = new Label("Match Date");
        MatchDate.setPadding(new Insets(0,220,0,0));
        MatchDate.setTextFill(Color.WHITE);
    
        MatchDate.setStyle("-fx-font-size:20px; -fx-font-weight: bold;");
        TextField MatchDateTextField = new TextField("20 Oct 2023");
        //CreateMatch.topToBottomTranslation(MatchDateTextField);
        setTextCenter(MatchDateTextField);

        Label MatchFormat = new Label("Match Format");
        MatchFormat.setPadding(new Insets(0,190,0,0));
        MatchFormat.setTextFill(Color.WHITE);

        MatchFormat.setStyle("-fx-font-size:20px; -fx-font-weight: bold;");
        TextField MatchFormatTextField = new TextField("T-20");
        //CreateMatch.topToBottomTranslation(MatchFormatTextField);
        setTextCenter(MatchFormatTextField);

        Label MatchVenue = new Label("Venue");
        MatchVenue.setPadding(new Insets(0,260,0,0));
        MatchVenue.setTextFill(Color.WHITE);

        MatchVenue.setStyle("-fx-font-size:20px; -fx-font-weight: bold;");
        TextField MatchVenueTextField = new TextField("MCA Pune");
        //CreateMatch.topToBottomTranslation(MatchVenueTextField);
        setTextCenter(MatchVenueTextField);

        Label TossWinner = new Label("Toss Winner");
        TossWinner.setPadding(new Insets(0,210,0,0));
        TossWinner.setTextFill(Color.WHITE);
        TossWinner.setStyle("-fx-font-size:20px; -fx-font-weight: bold;");
        TextField TossWinnerTextField = new TextField("Vikings");
        //CreateMatch.topToBottomTranslation(TossWinnerTextField);
        setTextCenter(TossWinnerTextField);

        

        Label Choose = new Label(" Choose Bat/Chase");
        Choose.setPadding(new Insets(0,160,0,0));
        Choose.setTextFill(Color.WHITE);
        Choose.setStyle("-fx-font-size:20px; -fx-font-weight: bold;");
        TextField ChooseTextField = new TextField("Bat");
        //CreateMatch.topToBottomTranslation(ChooseTextField);
        setTextCenter(ChooseTextField);

        Label Overs = new Label("Overs");
        Overs.setPadding(new Insets(0,270,0,0));
        Overs.setTextFill(Color.WHITE);
        Overs.setStyle("-fx-font-size:20px; -fx-font-weight: bold;");
        TextField OversTextField = new TextField("1");
        //CreateMatch.topToBottomTranslation(OversTextField);
        setTextCenter(OversTextField);

        Button AddMatch = new Button("Create Match");
        AddMatch.setAlignment(Pos.CENTER);
        AddMatch.setStyle("-fx-font-size:18px; -fx-background-color: #4CAF50; -fx-text-fill: white;");
        
        MatchDetails.getChildren().addAll(Mactch_Details,Match1Label,Team1NameTextField,Match2Label,Team2NameTextField,MatchDate,MatchDateTextField,MatchFormat,MatchFormatTextField,Overs,OversTextField,MatchVenue,MatchVenueTextField,TossWinner,TossWinnerTextField,Choose,ChooseTextField,AddMatch);
        MatchDetails.setAlignment(Pos.CENTER);

        VBox Team1Player = new VBox(20);
        Team1Player.setPadding(new Insets(0,0,0,0));
        Team1Player.setId("match-team1-vbox"); // Set the ID

        Button SubmitTeam1 = new Button(" Submit");
        SubmitTeam1.setStyle("-fx-font-size:18px; -fx-background-color: #4CAF50; -fx-text-fill: white;");


        Team1Players = new Label(Str + " Players");
        Team1Players.setStyle("-fx-font-size:37px; -fx-font-weight: bold;");
 
        TextField Player1TextField = new TextField("Dnyaneshwar Bichkule");
        //CreateMatch.bottomToLeftTranslation(Player1TextField);
        setTextCenter(Player1TextField);

        TextField Player2TextField = new TextField("Prashant Divekar");
        //CreateMatch.bottomToLeftTranslation(Player2TextField);
        setTextCenter(Player2TextField);
 
        TextField Player3TextField = new TextField("Kailas Dukare");
        //CreateMatch.bottomToLeftTranslation(Player3TextField);
        setTextCenter(Player3TextField);        
 
        TextField Player4TextField = new TextField("Shreyas Hambir");
        //CreateMatch.bottomToLeftTranslation(Player4TextField);
        Player4TextField.setMaxWidth(400);
        setTextCenter(Player4TextField);

        TextField Player5TextField = new TextField("Vishal Lohbhande");
        //CreateMatch.bottomToLeftTranslation(Player5TextField);
        setTextCenter(Player5TextField);
  
        TextField Player6TextField = new TextField("Shreyas Mangle");
        //CreateMatch.bottomToLeftTranslation(Player6TextField);
        setTextCenter(Player6TextField);
     
        TextField Player7TextField = new TextField("Atharva Jadhav");
        //CreateMatch.bottomToLeftTranslation(Player7TextField);
        setTextCenter(Player7TextField);
 
        TextField Player8TextField = new TextField("Piyush Verma");
        //CreateMatch.bottomToLeftTranslation(Player8TextField);
        setTextCenter(Player8TextField);

        TextField Player9TextField = new TextField("Rohit Korde");
        //CreateMatch.bottomToLeftTranslation(Player9TextField);
        setTextCenter(Player9TextField);

        TextField Player10TextField = new TextField("Rahul Hatkar");
        //CreateMatch.bottomToLeftTranslation(Player10TextField);
        setTextCenter(Player10TextField);

        TextField Player11TextField = new TextField("Shubham Kharke");
       // CreateMatch.bottomToLeftTranslation(Player11TextField);
        setTextCenter(Player11TextField);

        TextField Player12TextField = new TextField("Mihir Mandlik");
        //CreateMatch.bottomToLeftTranslation(Player12TextField);
        setTextCenter(Player12TextField);

        TextField Player13TextField = new TextField("Tohid Patwekar");
        //CreateMatch.bottomToLeftTranslation(Player13TextField);
        setTextCenter(Player13TextField);

        Team1Player.getChildren().addAll(Team1Players,Player1TextField,Player2TextField,Player3TextField,Player4TextField,Player5TextField,Player6TextField,Player7TextField,Player8TextField,Player9TextField,Player10TextField,Player11TextField,Player12TextField,Player13TextField,SubmitTeam1);
        Team1Player.setAlignment(Pos.CENTER);

        VBox Team2Player = new VBox(20);
        Team2Player.setPadding(new Insets(0,0,0,0));
        Team2Player.setId("match-team2-vbox");

        
        Button SubmitTeam2 = new Button("Submit");
        SubmitTeam2.setStyle("-fx-font-size:18px; -fx-background-color: #4CAF50; -fx-text-fill: white;");
        Team2Players = new Label(Str2 + " Players");
        Team2Players.setStyle("-fx-font-size:37px; -fx-font-weight: bold;");
        
        TextField Player14TextField = new TextField("Shashi Bagal");
        //CreateMatch.topToBottomTranslation(Player14TextField);
        setTextCenter(Player14TextField);

        TextField Player15TextField = new TextField("Sachin Patil");
       // CreateMatch.topToBottomTranslation(Player15TextField);
        setTextCenter(Player15TextField);

        TextField Player16TextField = new TextField("Akshay Jagtap");
       // CreateMatch.topToBottomTranslation(Player16TextField);
        setTextCenter(Player16TextField);

        TextField Player17TextField = new TextField(" Rahul Dabhade ");
       // CreateMatch.topToBottomTranslation(Player17TextField);
        setTextCenter(Player17TextField);

        TextField Player18TextField = new TextField(" Govind Dindewad ");
      //  CreateMatch.topToBottomTranslation(Player18TextField);
        setTextCenter(Player18TextField);

        TextField Player19TextField = new TextField(" Shivkumar Tengse ");
       // CreateMatch.topToBottomTranslation(Player19TextField);
        setTextCenter(Player19TextField);

        TextField Player20TextField = new TextField("Subodh ");
      //  CreateMatch.topToBottomTranslation(Player20TextField);
        setTextCenter(Player20TextField);

        TextField Player21TextField = new TextField(" Shubham Sangve ");
      //  CreateMatch.topToBottomTranslation(Player21TextField);
        setTextCenter(Player21TextField);

        TextField Player22TextField = new TextField("Rajkumar Chavhan ");
       // CreateMatch.topToBottomTranslation(Player22TextField);
        setTextCenter(Player22TextField);

        TextField Player23TextField = new TextField("Abhishekh Shinde ");
       // CreateMatch.topToBottomTranslation(Player23TextField);
        setTextCenter(Player23TextField);

        TextField Player24TextField = new TextField("Mayur Chavhan ");
     //   CreateMatch.topToBottomTranslation(Player24TextField);
        setTextCenter(Player24TextField);

        TextField Player25TextField = new TextField("Shubham Kharke ");
     //   CreateMatch.topToBottomTranslation(Player25TextField);
        setTextCenter(Player25TextField);

        TextField Player26TextField = new TextField("Aakash Khandagale ");
      //  CreateMatch.topToBottomTranslation(Player26TextField);
        setTextCenter(Player26TextField);

        Team2Player.getChildren().addAll(Team2Players,Player14TextField,Player15TextField,Player16TextField,Player17TextField,Player18TextField,Player19TextField,Player20TextField,Player21TextField,Player22TextField,Player23TextField,Player24TextField,Player25TextField,Player26TextField,SubmitTeam2);
        Team2Player.setAlignment(Pos.CENTER);

        VBox SelectPlayers = new VBox(20);
        SelectPlayers.setId("match-last-vbox");


        
        // String[] Arr2 = TeamPlayersdetail.Team1Details();
        ComboBox<String> Team1PlayerComboBox = new ComboBox<>();
        //CreateMatch.rightToLeftTranslation(Team1PlayerComboBox);
        Team1PlayerComboBox.setValue("Striker Batsman");
        Team1PlayerComboBox.setPrefSize(220,40);
        Team1PlayerComboBox.setStyle("-fx-font-size:17px; -fx-font-weight: bold;");
   
        // Team1PlayerComboBox.getItems().addAll(Arr2);
       
        //TextField SelectBatsmanTextField = new TextField(Team1PlayerComboBox);

        
        
        ComboBox<String> Team2PlayerComboBox = new ComboBox<>();
       // CreateMatch.rightToLeftTranslation(Team2PlayerComboBox);
        Team2PlayerComboBox.setValue("Non_Striker");
        Team2PlayerComboBox.setPrefSize(220,40);
        Team2PlayerComboBox.setStyle("-fx-font-size:17px; -fx-font-weight: bold;");


        ComboBox<String> SelectBowlerComboBox = new ComboBox<>();
       // CreateMatch.rightToLeftTranslation(SelectBowlerComboBox);
        SelectBowlerComboBox.setValue("Bowler");
        SelectBowlerComboBox.setPrefSize(220,40);
        SelectBowlerComboBox.setStyle("-fx-font-size:17px; -fx-font-weight: bold;");

        //TextField SelectBowlerTextField = new TextField();

        Button StartMatch = new Button("Start Match");
        
        StartMatch.setStyle("-fx-font-size:18px; -fx-background-color: #4CAF50; -fx-text-fill: white;");
        StartMatch.setOnAction(e -> {
            try {
                CricketScoreboardApp.ScoreBoard(primaryStage,BattingTeamName,BowlingTeamName,TossWinnerTextField.getText(),ChooseTextField.getText(),Team1PlayerComboBox.getValue(),Team2PlayerComboBox.getValue(),SelectBowlerComboBox.getValue(),BattingTeam,BowlingTeam,overs);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        SelectPlayers.getChildren().addAll(Team1PlayerComboBox,Team2PlayerComboBox,SelectBowlerComboBox,StartMatch);
        SelectPlayers.setAlignment(Pos.CENTER);


        SplitPane splitPane2 = new SplitPane(Team1Player,Team2Player);
        splitPane2.setDividerPositions(0.50);

        SplitPane splitPane3 = new SplitPane(splitPane2,SelectPlayers);
        splitPane3.setDividerPositions(0.70);
        
        SplitPane splitPane = new SplitPane(MatchDetails,splitPane3);
        splitPane.setDividerPositions(0.25);

        scene = new Scene(splitPane, 1920, 990);
        
        scene.getRoot().setStyle("-fx-background-color: #B0C4DE;");
        scene.getStylesheets().add("style.css");  
        window.setScene(scene);      
        window.show();
        
        String dbUrl = "jdbc:mysql://localhost:3306/crictrack";
        String dbUser = "root";
        String dbPassword = "Pass@123";
        
        final Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);

        //Button AddMatch = new Button("Start");

        

        AddMatch.setOnAction(e->{
            
            Team1 = Team1NameTextField.getText();
            Team2 = Team2NameTextField.getText();
            TossWinnerTeam = TossWinnerTextField.getText();
            MatchTime = MatchDateTextField.getText();
            Location = MatchVenueTextField.getText();
            String winner = "";
            String MatchFormat1 = MatchFormatTextField.getText();
            int TotalOvers = Integer.parseInt(OversTextField.getText());
            try {
        
                String insertQuery = "INSERT INTO matches (Team1, Team2, TossWinner, MatchTime, MatchLocation, Winner,MatchFormat,Overs) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    
            // Create a prepared statement
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
        
            // Set the values for the placeholders
            preparedStatement.setString(1, Team1);
            preparedStatement.setString(2, Team2);
            preparedStatement.setString(3, TossWinnerTeam);
            preparedStatement.setString(4, MatchTime);
            preparedStatement.setString(5, Location);
            preparedStatement.setString(6, winner);
            preparedStatement.setString(7,MatchFormat1);
            preparedStatement.setInt(8,TotalOvers);

            Team1Players.setText(Team1 + " Players");
            Team2Players.setText(Team2 + " Players");
            overs = TotalOvers;

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                System.out.println("Insert failed, no rows affected.");
            } else {
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    int primaryKey = generatedKeys.getInt(1);
                    Matchid = primaryKey;
                    System.out.println("Inserted with primary key: " + primaryKey);
                } else {
                    System.out.println("Insert failed, no ID obtained.");
                }
            }
            System.out.println("Successfully stored in the database.");
            }catch (SQLException ex) {
                ex.printStackTrace();
            }

            showSuccessPopup("Match Created Successfully!");
            

        });
        //String Str = Player10TextField.getText();
        
        
        SubmitTeam1.setOnAction(e -> {
            String Arr[] = new String[]{Player1TextField.getText(),Player2TextField.getText(),Player3TextField.getText(),Player4TextField.getText(),Player5TextField.getText(),Player6TextField.getText(),Player7TextField.getText(),Player8TextField.getText(),Player9TextField.getText(),Player10TextField.getText(),Player11TextField.getText(),Player12TextField.getText(),Player13TextField.getText()};
            no = 0;
            
            String selectQuery = "SELECT * FROM players";
                try (PreparedStatement preparedStatement = connection.prepareStatement(selectQuery)) {
                        ResultSet resultSet = preparedStatement.executeQuery();
                        while (resultSet.next()) {
                                playerid++;
                        }
                }
                catch (SQLException ex) {
                        ex.printStackTrace();
                }
            while(no < 13){
                //String Pname = Str;
                String TeamName = Team1NameTextField.getText();
                

                
                
                try {
                    // SQL INSERT statement
                    String insertQuery = "INSERT INTO players (PlayerId,Match_id,PlayerName,TeamName) " +
                        "VALUES (?,?, ?, ?)";
            
                    // Create a prepared statement
                    PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
                    
            
                    // Set the values for the placeholders
                    preparedStatement.setInt(1,playerid + no + 1);
                    preparedStatement.setInt(2, Matchid);
                    preparedStatement.setString(3, Arr[no]);
                    preparedStatement.setString(4, TeamName);

            
                    preparedStatement.executeUpdate();
                    

                    System.out.println("Succesfully Store DataBase");
            } catch (SQLException ex) {
                    ex.printStackTrace();
            }
            no++;

            }
            showSuccessPopup1("Team Submited!");
             
        });
        SubmitTeam2.setOnAction(e -> {
            
            while(no < 26){
                //int Matchid2 = Matchid;
                //String Pname = Str;
                String TeamName = Team2NameTextField.getText();
                String Arr2[] = new String[]{Player14TextField.getText(),Player15TextField.getText(),Player16TextField.getText(),Player17TextField.getText(),Player18TextField.getText(),Player19TextField.getText(),Player20TextField.getText(),Player21TextField.getText(),Player22TextField.getText(),Player23TextField.getText(),Player24TextField.getText(),Player25TextField.getText(),Player26TextField.getText()};
                

                
                
                try {
                    // SQL INSERT statement
                    String insertQuery = "INSERT INTO players (PlayerId,Match_id,PlayerName,TeamName) " +
                        "VALUES (?,?, ?, ?)";
            
                    // Create a prepared statement
                    PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
                    
            
                    // Set the values for the placeholders
                    preparedStatement.setInt(1,playerid + no + 1);
                    preparedStatement.setInt(2, Matchid);
                    preparedStatement.setString(3, Arr2[no - 13]);
                    preparedStatement.setString(4, TeamName);

            
                    preparedStatement.executeUpdate();

                    System.out.println("Succesfully Store DataBase");
            } catch (SQLException ex) {
                    ex.printStackTrace();
            }
            no++;

            }
            //CricketScoreboardApp.ScoreBoard(primaryStage);
            int temp1 = 0;
            Arr2 = TeamPlayersdetail.Team1Details(temp1,Matchid);
            temp1++;
            Arr3 = TeamPlayersdetail.Team1Details(temp1,Matchid);
            //Team1PlayerComboBox.getItems().addAll(Arr2[0],Arr2[1],Arr2[2],Arr2[3],Arr2[4],Arr2[5],Arr2[6],Arr2[7],Arr2[8],Arr2[9],Arr2[10],Arr2[11],Arr2[12]);
            

            if(TossWinnerTextField.getText().equals(Team1) && ChooseTextField.getText().equals("Bat")){
                Team1PlayerComboBox.getItems().addAll(Arr2[0],Arr2[1],Arr2[2],Arr2[3],Arr2[4],Arr2[5],Arr2[6],Arr2[7],Arr2[8],Arr2[9],Arr2[10],Arr2[11],Arr2[12]);
                Team2PlayerComboBox.getItems().addAll(Arr2[0],Arr2[1],Arr2[2],Arr2[3],Arr2[4],Arr2[5],Arr2[6],Arr2[7],Arr2[8],Arr2[9],Arr2[10],Arr2[11],Arr2[12]);
                SelectBowlerComboBox.getItems().addAll(Arr3[0],Arr3[1],Arr3[2],Arr3[3],Arr3[4],Arr3[5],Arr3[6],Arr3[7],Arr3[8],Arr3[9],Arr3[10],Arr3[11],Arr3[12]);
                BattingTeam = Arr2;
                BowlingTeam = Arr3;
                BattingTeamName = Team1;
                BowlingTeamName = Team2;
            }
            else if(TossWinnerTextField.getText().equals(Team1) && ChooseTextField.getText().equals("Chase")){
                SelectBowlerComboBox.getItems().addAll(Arr2[0],Arr2[1],Arr2[2],Arr2[3],Arr2[4],Arr2[5],Arr2[6],Arr2[7],Arr2[8],Arr2[9],Arr2[10],Arr2[11],Arr2[12]);
                Team1PlayerComboBox.getItems().addAll(Arr3[0],Arr3[1],Arr3[2],Arr3[3],Arr3[4],Arr3[5],Arr3[6],Arr3[7],Arr3[8],Arr3[9],Arr3[10],Arr3[11],Arr3[12]);
                Team2PlayerComboBox.getItems().addAll(Arr3[0],Arr3[1],Arr3[2],Arr3[3],Arr3[4],Arr3[5],Arr3[6],Arr3[7],Arr3[8],Arr3[9],Arr3[10],Arr3[11],Arr3[12]);
                BattingTeam = Arr3;
                BowlingTeam = Arr2;
                BattingTeamName = Team2;
                BowlingTeamName = Team1;
            }
            else if(TossWinnerTextField.getText().equals(Team2) && ChooseTextField.getText().equals("Bat")){
                Team1PlayerComboBox.getItems().addAll(Arr3[0],Arr3[1],Arr3[2],Arr3[3],Arr3[4],Arr3[5],Arr3[6],Arr3[7],Arr3[8],Arr3[9],Arr3[10],Arr3[11],Arr3[12]);
                Team2PlayerComboBox.getItems().addAll(Arr3[0],Arr3[1],Arr3[2],Arr3[3],Arr3[4],Arr3[5],Arr3[6],Arr3[7],Arr3[8],Arr3[9],Arr3[10],Arr3[11],Arr3[12]);
                SelectBowlerComboBox.getItems().addAll(Arr2[0],Arr2[1],Arr2[2],Arr2[3],Arr2[4],Arr2[5],Arr2[6],Arr2[7],Arr2[8],Arr2[9],Arr2[10],Arr2[11],Arr2[12]);
                BattingTeam = Arr3;
                BowlingTeam = Arr2;
                BattingTeamName = Team2;
                BowlingTeamName = Team1;

            }
            else if(TossWinnerTextField.getText().equals(Team2) && ChooseTextField.getText().equals("Chase")){
                SelectBowlerComboBox.getItems().addAll(Arr3[0],Arr3[1],Arr3[2],Arr3[3],Arr3[4],Arr3[5],Arr3[6],Arr3[7],Arr3[8],Arr3[9],Arr3[10],Arr3[11],Arr3[12]);
                Team1PlayerComboBox.getItems().addAll(Arr2[0],Arr2[1],Arr2[2],Arr2[3],Arr2[4],Arr2[5],Arr2[6],Arr2[7],Arr2[8],Arr2[9],Arr2[10],Arr2[11],Arr2[12]);
                Team2PlayerComboBox.getItems().addAll(Arr2[0],Arr2[1],Arr2[2],Arr2[3],Arr2[4],Arr2[5],Arr2[6],Arr2[7],Arr2[8],Arr2[9],Arr2[10],Arr2[11],Arr2[12]);
                BattingTeam = Arr2;
                BowlingTeam = Arr3;
                BattingTeamName = Team1;
                BowlingTeamName = Team2;

            }
            showSuccessPopup2("Team Submited!");
            //TeamPlayersdetail.Team1Details();
             
        });
    //     Pane root = new Pane();
    //     root.getChildren().addAll(Team1Players,Player1TextField,Player2TextField,Player3TextField,Player4TextField,Player5TextField,Player6TextField,Player7TextField,Player8TextField,Player9TextField,Player10TextField,Player11TextField,Player12TextField,Player13TextField,SubmitTeam1);
       
       
    //    primaryStage.setScene(scene);
    //    primaryStage.show();

    }
}
