
package Models;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ViewPlayerList {

    static Stage window;
    static Scene Scene;

    static TableView<PlayerList> tableView;
    static List<TableColumn<PlayerList, ?>> columns;
    static TableView<PlayerList> tableView2;
    static List<TableColumn<PlayerList, ?>> columns1;
    static TableView<PlayerList> tableView3;
    static List<TableColumn<PlayerList, ?>> columns2;
    static TableView<PlayerList> tableView4;
    static List<TableColumn<PlayerList, ?>> columns3;
    static TableView<PlayerList> tableView5;
    static List<TableColumn<PlayerList, ?>> columns4;

    static  VBox vBox;
    static VBox vBox2;

    
     public static void display(Stage primaryStage) {

        window = primaryStage;



        // Create TableColumns

        TableColumn<PlayerList, String> playernameColumn = new TableColumn<>("Player");
        playernameColumn.setMinWidth(200);
        playernameColumn.setCellValueFactory(new PropertyValueFactory<>("PlayerName"));
        playernameColumn.setStyle("-fx-font-size: 18px;");

        TableColumn<PlayerList,String> TeamnameColumn = new TableColumn<>("Team");
        TeamnameColumn.setMinWidth(150);
        TeamnameColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,String>("TeamName"));
        TeamnameColumn.setStyle("-fx-font-size: 18px;");
        
    //Run Scored

    TableColumn<PlayerList,Integer> RunScoredColumn = new TableColumn<>(" R ");
        RunScoredColumn.setMinWidth(80);
        RunScoredColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,Integer>("RunScored"));
        RunScoredColumn.setStyle("-fx-font-size: 18px;");

    // Balls faced

    TableColumn<PlayerList,Integer> BallsFacedColumn = new TableColumn<>(" B ");
        BallsFacedColumn.setMinWidth(80);
        BallsFacedColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,Integer>("BallsFaced"));
        BallsFacedColumn.setStyle("-fx-font-size: 18px;");
    
    // Sixes

    TableColumn<PlayerList,Integer> SixesColumn = new TableColumn<>(" 6s ");
        SixesColumn.setMinWidth(80);
        SixesColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,Integer>("Sixes"));
        SixesColumn.setStyle("-fx-font-size: 18px;");
    
    // Fours
    
    TableColumn<PlayerList,Integer> FoursColumn = new TableColumn<>(" 4s ");
        FoursColumn.setMinWidth(80);
        FoursColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,Integer>("Fours"));
        FoursColumn.setStyle("-fx-font-size: 18px;");
    
    //Strike rate 
        TableColumn<PlayerList,String> StrikeRateColumn = new TableColumn<>(" SR ");
        StrikeRateColumn.setMinWidth(80);
        StrikeRateColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,String>("StrikeRate"));
        StrikeRateColumn.setStyle("-fx-font-size: 18px;");

    
    //DismisalType
        TableColumn<PlayerList,String> DismisalTypeColumn = new TableColumn<>("Status");
        DismisalTypeColumn.setMinWidth(120);
        DismisalTypeColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,String>("DismisalType"));
        DismisalTypeColumn.setStyle("-fx-font-size: 18px;");
    
    //Wickets
        TableColumn<PlayerList,Integer> WicketsColumn = new TableColumn<>(" W ");
        WicketsColumn.setMinWidth(50);
        WicketsColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,Integer>("Wickets"));
        WicketsColumn.setStyle("-fx-font-size: 18px;");

    //OversBowled
        TableColumn<PlayerList,Double> OversBowledColumn = new TableColumn<>(" O ");
        OversBowledColumn.setMinWidth(50);
        OversBowledColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,Double>("OversBowled"));
        OversBowledColumn.setStyle("-fx-font-size: 18px;");

    //Economy
        TableColumn<PlayerList,String> EconomyColumn = new TableColumn<>(" Eco ");
        EconomyColumn.setMinWidth(50);
        EconomyColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,String>("Economy")); 
        EconomyColumn.setStyle("-fx-font-size: 18px;");
    
    //RunsConceeded
        TableColumn<PlayerList,String> RunsConceededColumn = new TableColumn<>(" R ");
        RunsConceededColumn.setMinWidth(50);
        RunsConceededColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,String>("RunsConceeded")); 
        RunsConceededColumn.setStyle("-fx-font-size: 18px;");

    //extra column
        TableColumn<PlayerList,String> extraColumn = new TableColumn<>(" . ");
        extraColumn.setMinWidth(50);
        //extraColumn.setCellValueFactory(new PropertyValueFactory<PlayerList,String>("Economy")); 
        extraColumn.setStyle("-fx-font-size: 18px;");

        // Create a list to store the columns
        columns = new ArrayList<>();
        columns1 = new ArrayList<>();
        columns2 = new ArrayList<>();
        columns3 = new ArrayList<>();
        columns4 = new ArrayList<>();
        
        // columns for all mix players 
        
        columns.add(playernameColumn);
        columns.add(TeamnameColumn);
        columns.add(RunScoredColumn);
        columns.add(BallsFacedColumn);
        columns.add(FoursColumn);
        columns.add(SixesColumn);
        columns.add(StrikeRateColumn);
        columns.add(DismisalTypeColumn);
        columns.add(WicketsColumn);
        columns.add(OversBowledColumn);
        columns.add(EconomyColumn);
        columns.add(RunsConceededColumn);
        
        tableView = new TableView<>();
        tableView.setItems(Scorecard1.getPlayersDetailsList());


        tableView.getColumns().addAll(columns);
        tableView.setVisible(true);
        //Scorecard1.clearl();
       

        
        // Inning 1 batting Tabel
        columns1.add(extraColumn);
        columns1.add(playernameColumn);
        columns1.add(RunScoredColumn);
        columns1.add(BallsFacedColumn);
        columns1.add(FoursColumn);
        columns1.add(SixesColumn);
        columns1.add(StrikeRateColumn);
        columns1.add(DismisalTypeColumn);


        tableView2 = new TableView<>();
        tableView2.setItems(Scorecard1.getTeam1List());
        tableView2.getColumns().addAll(columns1);
        tableView2.setVisible(true);
        tableView2.setMinHeight(500);

        //Scorecard1.clearl();

        // Inning 1 bowling Tabel

        // columns2.add(playernameColumn);
        // columns2.add(WicketsColumn);
        // columns2.add(OversBowledColumn);
        // columns2.add(EconomyColumn);
        // columns2.add(RunsConceededColumn);

        // tableView4 = new TableView<>();
        // //tableView4.setItems(Scorecard1.getPlayersDetailsList(4));
        // tableView4.getColumns().addAll(columns2);
        // tableView4.setVisible(true);
        //Scorecard1.clearl();

       

        // // Inning 2 Batting Table column
        
        columns3.add(extraColumn);
        columns3.add(playernameColumn);
        columns3.add(RunScoredColumn);
        columns3.add(BallsFacedColumn);
        columns3.add(FoursColumn);
        columns3.add(SixesColumn);
        columns3.add(StrikeRateColumn);
        columns3.add(DismisalTypeColumn);

        
        tableView3 = new TableView<>();
        tableView3.setItems(Scorecard1.getTeam2List());
        tableView3.getColumns().addAll(columns3);
        tableView3.setVisible(true);
        tableView3.setMinHeight(500);
        //Scorecard1.clearl();

        // // Inning 2 bowling Tabel

        // columns4.add(playernameColumn);
        // columns4.add(WicketsColumn);
        // columns4.add(OversBowledColumn);
        // columns4.add(EconomyColumn);
        // columns4.add(RunsConceededColumn);

        
        // tableView5 = new TableView<>();
        // tableView5.setItems(Scorecard1.getPlayersDetailsList(5));
        // tableView5.getColumns().addAll(columns4);
        // tableView5.setVisible(true);

        // Add Table view
        vBox = new VBox();
        
        vBox2 = new VBox();
       

       
        
    

        
        vBox.getChildren().add(tableView2);
       
        vBox2.getChildren().add(tableView3);

        SplitPane splitPane = new SplitPane();
        splitPane.setDividerPositions(0.5);
        splitPane.getItems().addAll(vBox,vBox2);

        //StackPane stackPane = new StackPane();

        HBox hBox = new HBox();
        hBox.getChildren().addAll(vBox,vBox2);
        
        // VBox vBox3 = new VBox();
        // vBox3.getChildren().add(tableView2);
        // VBox vBox4 = new VBox();
        // vBox4.getChildren().add(tableView3);
        // HBox hBox2 = new HBox();
        // hBox2.getChildren().addAll(vBox3,vBox4);

        Button Team1battingdata = new Button("Team1BattingData");

        Team1battingdata.setOnAction(e->{

        
        });
        Button Team2battingdata = new Button("Team1BattingData");
        Team2battingdata.setOnAction(e->{

        
        });
        Button Team1ballingdata = new Button("Team1BattingData");
        Team1ballingdata.setOnAction(e->{

        
        });
        Button Team2ballingdata = new Button("Team1BattingData");
        Team2ballingdata.setOnAction(e->{

        
        });

        VBox vBox5 = new VBox();
        vBox5.getChildren().addAll(hBox);
        Scene scene = new Scene(splitPane, 1920, 1080);

        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.show();

   
    }

}
