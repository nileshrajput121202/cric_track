package Models;

import javafx.scene.control.Label;

import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ScoreCard {
    
    private static Label Team1;
    private static Label Team2;
    private static Label Score;
    private static Label ballname;
    private static Label RunRateLabel;
    private static Label batBoldLabel;
    private static Label batters;
    private static Label bat1Label;
    private static Label bat2Label;
    private static VBox ballNameVBox;
    private static VBox dataVBox;
    static int i = 1;

    private static Font boldFont = Font.font("Times new Roman", FontWeight.BOLD, 22);
    public static VBox TeamDisp(String team1,String team2){
   
        VBox nameBox = new VBox(20);
        Team1 = new Label();
        Team2 = new Label();
        
        Team2.setText(team2);
        Team1.setText(team1);  
        Team1.setFont(boldFont);
        Team2.setFont(boldFont);
        Team1.setStyle("-fx-font-size:50px; -fx-font-weight:bold; -fx-text-fill: white;");
        Team2.setStyle("-fx-font-size:50px; -fx-font-weight:bold; -fx-text-fill: white;");

        nameBox.getChildren().addAll(Team1,Team2);
        return  nameBox;   
    }
    public static VBox TeamDisp2(String team1,String team2,int inning1wickets,int inning1balls,int inning1overs,int target){
   
        VBox nameBox = new VBox(20);
        Team1 = new Label();
        Team2 = new Label();
        Team1.setFont(boldFont);
        Team2.setFont(boldFont);
        Team2.setText(team2 + "          "+target + " / "+ inning1wickets + "( " + inning1overs + "." + inning1balls + " )" );
        Team1.setText(team1 + "          "+target + " / "+ inning1wickets + "( " + inning1overs + "." + inning1balls + " )"); 
        Team1.setStyle("-fx-font-size:50px; -fx-font-weight:bold; -fx-text-fill: white;");
        Team2.setStyle("-fx-font-size:50px; -fx-font-weight:bold; -fx-text-fill: white;");

        nameBox.getChildren().addAll(Team1,Team2);
        return  nameBox;
    }

    public static Label DispScore(int score, int wickets, double overs){
        Score = new Label();
        Score.setFont(boldFont);
        Score.setText("                "+score+" / "+wickets+" ("+overs+")");
        Score.setStyle("-fx-font-size:30px");
        return Score; 
    }
    public static Label DispScore2(int score, int wickets, double overs){
        Score = new Label();
        Score.setFont(boldFont);
        Score.setText("\n  "+score+" / "+wickets+" ("+overs+")");
        Score.setStyle("-fx-font-size:30px");
        return Score; 
    }
    public static Label runrateDisp(double RR){

        RunRateLabel = new Label("Current Run Rate : "+RR);
        RunRateLabel.setStyle("-fx-font-size:20px");
        return RunRateLabel;  
    }

    public static VBox updateRuns(int runs, int ball, int four, int six,double SR,int i){
        
        batBoldLabel = new Label();
        batBoldLabel.setText("  R                B                4s                6s                SR");
        batBoldLabel.setFont(boldFont);

        bat1Label = new Label();
        bat2Label = new Label();
        if(i == 0){
            bat1Label.setText("  "+runs+"                     "+ball+"                     "+four+"                       "+six+"                      "+SR);
        }
        else{
            bat2Label.setText("  "+runs+"                     "+ball+"                     "+four+"                       "+six+"                      "+SR);
        }

        dataVBox = new VBox(10);
        dataVBox.getChildren().addAll(batBoldLabel,bat1Label,bat2Label);
        return dataVBox;
    }
    public static VBox ballersInfo(String str1){

        batters = new Label("Ballers");
        batters.setFont(boldFont);
        ballname = new Label(str1);

        ballNameVBox = new VBox(10);
        ballNameVBox.getChildren().addAll(batters,ballname);
    
        return ballNameVBox;  
    }
    // public static VBox updateBalls(double overs, int m, int runs,int wicket,double ECO){
        
    //     ballBoldLabel = new Label();
    //     ballBoldLabel.setText("O                M                R                W                ECO");
    //     ballBoldLabel.setFont(boldFont);

    //     ball1Label = new Label();
    //     ball1Label.setText(overs+"                   "+m+"                   "+runs+"                       "+wicket+"                      "+ECO);
        
    //     ballDataVBox = new VBox(10);
    //     ballDataVBox.getChildren().addAll(ballBoldLabel,ball1Label);
    //     return ballDataVBox;
    // }
    // public static VBox updatelastBalls(double overs, int m, int runs,int wicket,double ECO){

    //     ball1Label = new Label();
    //     ball1Label.setText("  "+overs+"                   "+m+"                   "+runs+"                       "+wicket+"                      "+ECO);
        
    //     ballDataVBox = new VBox(5);
    //     ballDataVBox.getChildren().addAll(ball1Label);
    //     return ballDataVBox;
    // }
    public static VBox baller2Info(String str1){

        ballname = new Label(str1);

        ballNameVBox = new VBox(5);
        ballNameVBox.getChildren().addAll(ballname);
    
        return ballNameVBox;  
    }
 


    
    public static String TeamRunRate(int totalruns,int overs,int balls) {
        double economy1 = 0.0;
        
        
        double Overs = overs + (balls / 6.0); // Calculate total overs bowled by Bowler
        
        if (Overs > 0) {
            economy1 =  totalruns / Overs;
            Label runRateLabel = new Label("Current Run Rate : " + String.format("%.2f", economy1));
            return runRateLabel.getText();
        } else {
            economy1 = 0.0;
            Label l1 = new Label("0.0");
            return l1.getText();
        }
        

    }

    
    public static String calculateRequiredRunRate(int targetScore, int currentScore, double oversRemaining) {
        if (oversRemaining > 0) {
            Label RequiredRunRate = new Label("Required Run Rate : " + String.format("%.2f",(targetScore - currentScore) / oversRemaining));
            return RequiredRunRate.getText();
        } else {
            
            Label l1 = new Label("Required Run Rate : 0.0");
            return l1.getText();
        }
    }

}

